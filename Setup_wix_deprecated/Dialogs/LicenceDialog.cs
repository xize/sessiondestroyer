using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using io = System.IO;

using WixSharp;
using WixSharp.UI.Forms;

namespace WixSharpSetup.Dialogs
{
    /// <summary>
    /// The standard Licence dialog
    /// </summary>
    public partial class LicenceDialog : ManagedForm, IManagedDialog // change ManagedForm->Form if you want to show it in designer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenceDialog"/> class.
        /// </summary>
        public LicenceDialog()
        {
            InitializeComponent();
        }

        void LicenceDialog_Load(object sender, EventArgs e)
        {
            accepted.Checked = Runtime.Session["LastLicenceAcceptedChecked"] == "True";

            ResetLayout();
        }

        void ResetLayout()
        {
            this.agreement2.ForeColor = Color.Gray;
        }

        void back_Click(object sender, EventArgs e)
        {
            Shell.GoPrev();
        }

        void next_Click(object sender, EventArgs e)
        {
            Shell.GoNext();
        }

        void cancel_Click(object sender, EventArgs e)
        {
            Shell.Cancel();
        }

        void accepted_CheckedChanged(object sender, EventArgs e)
        {
            next.Enabled = accepted.Checked;
            Runtime.Session["LastLicenceAcceptedChecked"] = accepted.Checked.ToString();
        }

        void print_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Path.Combine(Path.GetTempPath(), Runtime.Session.Property("ProductName") + ".licence.rtf");
                io.File.WriteAllText(file, agreement2.Rtf);
                Process.Start(file);
            }
            catch { }
        }

        void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var data = new DataObject();

                if (agreement2.SelectedText.Length > 0)
                {
                    data.SetData(DataFormats.UnicodeText, agreement2.SelectedText);
                    data.SetData(DataFormats.Rtf, agreement2.SelectedRtf);
                }
                else
                {
                    data.SetData(DataFormats.Rtf, agreement2.Rtf);
                    data.SetData(DataFormats.Text, agreement2.Text);
                }

                Clipboard.SetDataObject(data);
            }
            catch { }
        }
    }
}