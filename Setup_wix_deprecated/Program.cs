﻿using System;
using WixSharp;
using WixSharpSetup.Dialogs;

// NuGet console: Install-Package WixSharp
// NuGet Manager UI: browse tab

namespace Setup2
{
    class Program
    {
        static void Main()
        {
            var project = new ManagedProject("SessionDestroyer",
                             new Dir(@"%ProgramFiles%\0c3\SessionDestroyer",
                                 new File(@"..\SessionDestroyer\bin\Release\SessionDestroyer.exe")));

            project.GUID = new Guid("B0E41381-95EE-4CA9-87C5-F657E0D69859");
            project.UpgradeCode = new Guid("F0E31381-95EE-4CA9-87C5-F657E0D69859");
            project.Version = new Version("1.0.3.8");

            //custom set of standard UI dialogs
            project.ManagedUI = new ManagedUI();

            project.ManagedUI.InstallDialogs.Add<WelcomeDialog>()
                                            .Add<LicenceDialog>()
                                            .Add<InstallDirDialog>()
                                            .Add<ProgressDialog>()
                                            .Add<ExitDialog>();

            project.ManagedUI.ModifyDialogs//Add<MaintenanceTypeDialog>()
                                           //Add<FeaturesDialog>()
                                           .Add<ProgressDialog>()
                                           .Add<ExitDialog>();

            //project.SourceBaseDir = "<input dir path>";
            //project.OutDir = "<output dir path>";

            ValidateAssemblyCompatibility();

            project.BuildMsi();
        }

        static void ValidateAssemblyCompatibility()
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            if (!assembly.ImageRuntimeVersion.StartsWith("v2."))
            {
                Console.WriteLine("Warning: assembly '{0}' is compiled for {1} runtime, which may not be compatible with the CLR version hosted by MSI. " +
                                  "The incompatibility is particularly possible for the EmbeddedUI scenarios. " +
                                   "The safest way to solve the problem is to compile the assembly for v3.5 Target Framework.",
                                   assembly.GetName().Name, assembly.ImageRuntimeVersion);
            }
        }
    }
}