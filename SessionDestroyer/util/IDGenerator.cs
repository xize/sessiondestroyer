﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace SessionDestroyer.util
{
    public class IDGenerator
    {

        private static IDGenerator inst;

        public IDGenerator()
        {

        }


        public String GenerateID(String ipaddress)
        {
            if (ipaddress.ToLower().Contains("error"))
                return "ERROR";
           return Convert.ToBase64String(Encoding.UTF8.GetBytes(ipaddress));
        }

        public String GenerateLocalID()
        {
            String ip = null;
            try
            {
                using (Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
                {
                    s.Connect("8.8.8.8", 6680);
                    IPEndPoint ipe = s.LocalEndPoint as IPEndPoint;
                    Regex regex = new Regex(@":\d+");
                    ip = regex.Replace(ipe.ToString(), "");
                }
            } catch(Exception)
            {
                ip = "ERROR";
                return ip;
            }
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(ip));
        }

        public string DecryptID(String id)
        {
            try
            {
                return Encoding.UTF8.GetString(Convert.FromBase64String(id));
            } catch(Exception)
            {
                return null;
            }
        }

        public ISet<String> GetAllIpsFromList(ListView list)
        {
            ISet<String> ips = new HashSet<String>();
            foreach(ListViewItem item in list.Items)
            {
                String ip = DecryptID(item.SubItems[1].Text);
                ips.Add(ip);
            }
            return ips;
        }

        public static IDGenerator GetFactory()
        {
            if(inst == null)
            {
                inst = new IDGenerator();
            }
            return inst;
        }

    }
}
