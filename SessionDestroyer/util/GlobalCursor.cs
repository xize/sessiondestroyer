﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SessionDestroyer.util
{
    class GlobalCursor
    {
        [DllImport("user32.dll", EntryPoint = "SystemParametersInfo")]
        public static extern bool SystemParametersInfo(uint uiAction, uint uiParam, uint pvParam, uint fWinIni);



        const int SPI_SETCURSORS = 0x0057;

        const int SPIF_UPDATEINIFILE = 0x01;

        const int SPIF_SENDCHANGE = 0x02;


        private String DefaultCursorPath = @"%SystemRoot%\cursors\aero_arrow.cur";
        private String CrossCursorPath = @"%SystemRoot%\cursors\cross_rm.cur";

        //SystemParametersInfo(SPI_SETCURSORS, 0, null, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);

        public void ForceGlobalCross()
        {
            RegistryKey k = Registry.CurrentUser.OpenSubKey(@"Control Panel").OpenSubKey("Cursors", true);
            k.SetValue("Arrow", this.CrossCursorPath);
            k.Close();

            SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
        }

        public void UndoGlobalCross()
        {
            RegistryKey k = Registry.CurrentUser.OpenSubKey(@"Control Panel").OpenSubKey("Cursors", true);
            k.SetValue("Arrow", this.DefaultCursorPath);
            k.Close();

            SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
        }

    }
}
