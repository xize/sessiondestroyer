﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer
{
    public partial class WifiProfileWindow : Form
    {
        private TextBox textbox;

        public WifiProfileWindow(TextBox textbox)
        {
            this.textbox = textbox;
            InitializeComponent();
        }

        private void WifiProfileWindow_Load(object sender, EventArgs e)
        {
            GenerateProfiles();
        }

        public async void GenerateProfiles()
        {
            await Task.Run(() =>
            {
                string tmp = Path.GetTempPath();

                ProcessStartInfo pinfo = new ProcessStartInfo("cmd");
                pinfo.Arguments = @"/c netsh wlan show profiles > " + tmp + "snetwork.txt";
                pinfo.CreateNoWindow = true;
                pinfo.UseShellExecute = false;
                Process p = Process.Start(pinfo);
                p.WaitForExit(); //could be bad :)

                string output = File.ReadAllText(tmp + "snetwork.txt");

                if(output.Split('\n').Length <= 2)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.Close();
                    }));
                    
                    MessageBox.Show("it looks that you don't have any wireless connections, please check this.\n\nCommand: netsh wlan show profiles\nOutput: "+output, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ParseAllPolicyProfiles(output);
                ParseAllProfiles(output);



                this.Invoke(new MethodInvoker(delegate
                {
                    int count1 = 0;
                    TreeNode node1 = treeView1.Nodes.Find("policyprofiles", false)[0];
                    count1 = node1.Nodes.Count;
                    node1.Text = String.Format(node1.Text, count1);

                    int count2 = 0;
                    TreeNode node2 = treeView1.Nodes.Find("alluserprofiles", false)[0];
                    count2 = node2.Nodes.Count;
                    node2.Text = String.Format(node2.Text, count2);

                    this.treeView1.Update();
                }));
            });

        }

        private void ParseAllPolicyProfiles(string output)
        {
                //parse normal profiles
                int firstindex = output.IndexOf("---------------------------------\r");

                output = output.Substring(firstindex);
                int lastindex = output.LastIndexOf("User profiles\r");
                output = output.Substring(0, lastindex);

                if (output.ToLower().Contains("none"))
                    return;

                string[] outputdata = output.Split('\n');

                foreach (string s in outputdata)
                {
                    if (s == "" || s == " " || s == "\r")
                        break;

                    if (!s.StartsWith("-"))
                    {

                        string newv = s;
                        //trim whitespace!
                        newv = s.Trim();
                        newv = newv.Split(':')[1].Trim();

                        this.Invoke(new MethodInvoker(delegate
                        {
                            TreeNode node = treeView1.Nodes.Find("policyprofiles", false)[0];
                            node.Nodes.Add(newv);
                        }));
                    }
                }
        }

        private void ParseAllProfiles(string output)
        {
                //parse normal profiles
                int lastindex = output.LastIndexOf("-------------\r");
                output = output.Substring(lastindex);

                string[] outputdata = output.Split('\n');

                foreach (string s in outputdata)
                {
                    if (s == "" || s == " " || s == "\r")
                        break;

                    if (!s.StartsWith("-"))
                    {

                        string newv = s;
                        //trim whitespace!
                        newv = s.Trim();
                        newv = newv.Split(':')[1].Trim();

                        this.Invoke(new MethodInvoker(delegate
                        {
                            TreeNode node = treeView1.Nodes.Find("alluserprofiles", false)[0];
                            node.Nodes.Add(newv);
                        }));
                    }
                }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent != null)
            {
                if (e.Node.Parent.Name == "policyprofiles")
                {
                    this.textbox.Text = e.Node.Text;
                    this.Close();
                }
                else if (e.Node.Parent.Name == "alluserprofiles")
                {
                    this.textbox.Text = e.Node.Text;
                    this.Close();
                }
            }
        }
    }
}
