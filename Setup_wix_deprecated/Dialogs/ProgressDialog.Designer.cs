using WixSharp;
using WixSharp.UI.Forms;

namespace WixSharpSetup.Dialogs
{
    partial class ProgressDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progress = new System.Windows.Forms.ProgressBar();
            this.currentAction = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.description = new System.Windows.Forms.Label();
            this.dialogText = new System.Windows.Forms.Label();
            this.waitPrompt = new System.Windows.Forms.Label();
            this.roundedCornersControl1 = new SessionDestroyer.controls.RoundedCornersControl();
            this.roundedCornersControl2 = new SessionDestroyer.controls.RoundedCornersControl();
            this.roundedCornersControl3 = new SessionDestroyer.controls.RoundedCornersControl();
            this.SuspendLayout();
            // 
            // progress
            // 
            this.progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progress.Location = new System.Drawing.Point(32, 165);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(434, 13);
            this.progress.Step = 1;
            this.progress.TabIndex = 20;
            // 
            // currentAction
            // 
            this.currentAction.AutoSize = true;
            this.currentAction.Location = new System.Drawing.Point(34, 144);
            this.currentAction.Name = "currentAction";
            this.currentAction.Size = new System.Drawing.Size(0, 13);
            this.currentAction.TabIndex = 19;
            // 
            // back
            // 
            this.back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.back.AutoSize = true;
            this.back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Enabled = false;
            this.back.FlatAppearance.BorderSize = 0;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.back.ForeColor = System.Drawing.Color.White;
            this.back.Location = new System.Drawing.Point(228, 326);
            this.back.MinimumSize = new System.Drawing.Size(75, 0);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(79, 25);
            this.back.TabIndex = 0;
            this.back.Text = "[WixUIBack]";
            this.back.UseVisualStyleBackColor = false;
            // 
            // next
            // 
            this.next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.next.AutoSize = true;
            this.next.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next.Enabled = false;
            this.next.FlatAppearance.BorderSize = 0;
            this.next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.next.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.next.ForeColor = System.Drawing.Color.White;
            this.next.Location = new System.Drawing.Point(313, 326);
            this.next.MinimumSize = new System.Drawing.Size(75, 0);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(77, 25);
            this.next.TabIndex = 0;
            this.next.Text = "[WixUINext]";
            this.next.UseVisualStyleBackColor = false;
            // 
            // cancel
            // 
            this.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancel.AutoSize = true;
            this.cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancel.FlatAppearance.BorderSize = 0;
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.cancel.ForeColor = System.Drawing.Color.White;
            this.cancel.Location = new System.Drawing.Point(395, 326);
            this.cancel.MinimumSize = new System.Drawing.Size(75, 0);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(87, 25);
            this.cancel.TabIndex = 0;
            this.cancel.Text = "[WixUICancel]";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.BackColor = System.Drawing.Color.Transparent;
            this.description.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.description.ForeColor = System.Drawing.Color.LightGray;
            this.description.Location = new System.Drawing.Point(29, 131);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(157, 11);
            this.description.TabIndex = 16;
            this.description.Text = "[ProgressDlgTextInstalling]";
            // 
            // dialogText
            // 
            this.dialogText.BackColor = System.Drawing.Color.Transparent;
            this.dialogText.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dialogText.ForeColor = System.Drawing.Color.Gray;
            this.dialogText.Location = new System.Drawing.Point(29, 144);
            this.dialogText.Name = "dialogText";
            this.dialogText.Size = new System.Drawing.Size(132, 13);
            this.dialogText.TabIndex = 19;
            this.dialogText.Text = "[ProgressDlgStatusLabel]";
            this.dialogText.Visible = false;
            // 
            // waitPrompt
            // 
            this.waitPrompt.AutoSize = true;
            this.waitPrompt.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitPrompt.ForeColor = System.Drawing.Color.Gray;
            this.waitPrompt.Location = new System.Drawing.Point(29, 194);
            this.waitPrompt.Name = "waitPrompt";
            this.waitPrompt.Size = new System.Drawing.Size(260, 33);
            this.waitPrompt.TabIndex = 23;
            this.waitPrompt.TabStop = true;
            this.waitPrompt.Text = "Please wait for UAC prompt to appear.\r\n\r\nIf it appears minimized then activate it" +
    " from the taskbar.";
            this.waitPrompt.Visible = false;
            // 
            // roundedCornersControl1
            // 
            this.roundedCornersControl1.Radius = 8;
            this.roundedCornersControl1.TargetControl = this.back;
            // 
            // roundedCornersControl2
            // 
            this.roundedCornersControl2.Radius = 8;
            this.roundedCornersControl2.TargetControl = this.next;
            // 
            // roundedCornersControl3
            // 
            this.roundedCornersControl3.Radius = 8;
            this.roundedCornersControl3.TargetControl = this.cancel;
            // 
            // ProgressDialog
            // 
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Setup.Properties.Resources.layout2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(494, 361);
            this.ControlBox = false;
            this.Controls.Add(this.back);
            this.Controls.Add(this.waitPrompt);
            this.Controls.Add(this.next);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.currentAction);
            this.Controls.Add(this.description);
            this.Controls.Add(this.dialogText);
            this.Name = "ProgressDialog";
            this.Text = "[ProgressDlg_Title]";
            this.Load += new System.EventHandler(this.ProgressDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label description;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Label currentAction;
        private System.Windows.Forms.Label dialogText;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Label waitPrompt;
        private SessionDestroyer.controls.RoundedCornersControl roundedCornersControl1;
        private SessionDestroyer.controls.RoundedCornersControl roundedCornersControl2;
        private SessionDestroyer.controls.RoundedCornersControl roundedCornersControl3;
    }
}