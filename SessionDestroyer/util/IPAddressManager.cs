﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SessionDestroyer.util
{
    public class IPAddressManager
    {
        private List<String> whitelist = new List<String>();
        public IPAddressManager(List<String> whitelist)
        {
            this.whitelist = whitelist;

            //order ipaddresses
            this.whitelist.Sort(IPSort);
   
        }

        private int IPSort(string x, string y)
        {
            int xindex = GenerateSortIndex(x);
            int yindex = GenerateSortIndex(y);

            if(xindex > yindex)
            {
                return 1;
            } else if(xindex < yindex)
            {
                return -1;
            } else
            {
                return 0;
            }
        }

        private int GenerateSortIndex(String s)
        {
            String[] octals = s.Split('.');
            int octal1 = int.Parse(octals[0]);
            int octal2 = int.Parse(octals[1]);
            int octal3 = int.Parse(octals[2]);
            int octal4 = int.Parse(octals[3]);

            return
                octal1 * 256 * 256 * 256 +
                octal2 * 256 * 256 +
                octal3 * 256 +
                octal4;
        }

        public String BuildIPString()
        {
            String start = "1.1.1.1-";
            String end = "255.255.255.254";

            String currentposition = start;

            StringBuilder build = new StringBuilder();
            foreach(String ip in this.whitelist)
            {

                String[] data = ip.Split('.');
                int octal1 = int.Parse(data[0]);
                int octal2 = int.Parse(data[1]);
                int octal3 = int.Parse(data[2]);
                int octal4 = int.Parse(data[3]);

                if(octal4 < 255)
                {
                    octal4 += 1;
                } else
                {
                    octal4 = 1;
                    if(octal3 < 255)
                    {
                        octal3 += 1;
                    } else
                    {
                        octal3 = 1;
                        if(octal2 < 255)
                        {
                            octal2 += 1;
                        } else
                        {
                            octal2 = 1;
                            if(octal1 < 255)
                            {
                                octal1 += 1;
                            } else
                            {
                                break;
                            }
                        }
                    }
                }

                String next = octal1 + "." + octal2 + "." + octal3 + "." + octal4;

                if (octal4-2 > 0)
                {
                    octal4 -= 2;
                }
                else
                {
                    if (octal3-2 > 0)
                    {
                        octal3 -= 2;
                    }
                    else
                    {
                        if (octal2-2 > 0)
                        {
                            octal2 -= 2;
                        }
                        else
                        {
                            if (octal1-2 > 0)
                            {
                                octal1 -= 2;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                String previous = octal1+"."+octal2+"."+octal3+"."+octal4;

                build.Append(currentposition+previous+",");
                currentposition = next + "-";
            }

            if(this.whitelist.Count == 0)
            {
                return "";
            }

            return build.ToString()+currentposition+end;
        }

    }
}
