﻿using NetFwTypeLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer.util
{
    class MyFirewall
    {

        public static int FW_BOTH = 256;
        public static int TCP = 6;
        public static int UDP = 17;

        public bool HasRules()
        {
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            IEnumerator it = firewallPolicy.Rules.GetEnumerator();

            while (it.MoveNext())
            {
                INetFwRule2 fwr = (INetFwRule2)it.Current;
                if (fwr.Name.ToLower().Equals("GAME_BLOCK".ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

        public void BlockPorts(int startport, int endport, int protocol, ISet<String> remote_ips = null, ISet<String> local_ips = null)
        {
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            IEnumerator it = firewallPolicy.Rules.GetEnumerator();

            while(it.MoveNext())
            {
                INetFwRule2 fwr =  (INetFwRule2)it.Current;
                if(fwr.Name.ToLower().Equals("GAME_BLOCK".ToLower()))
                {
                    firewallPolicy.Rules.Remove(fwr.Name);
                }
            }

            Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
            INetFwPolicy2 fwPolicy2 = (INetFwPolicy2)Activator.CreateInstance(tNetFwPolicy2);
            var currentProfiles = fwPolicy2.CurrentProfileTypes;

            IPAddressManager localmanager = new IPAddressManager(local_ips.ToList());
            IPAddressManager remotemanager = new IPAddressManager(remote_ips.ToList());

            String local = localmanager.BuildIPString();
            String remote = remotemanager.BuildIPString();

            
            INetFwRule2 inboundRule = (INetFwRule2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
            inboundRule.RemoteAddresses = remote;
            inboundRule.LocalAddresses = local;
            inboundRule.Profiles = currentProfiles;
            inboundRule.Protocol = protocol;
            inboundRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
            inboundRule.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK;
            inboundRule.Name = "GAME_BLOCK";
            inboundRule.Enabled = true;
            if (protocol != FW_BOTH)
            {
                /**
                we do not use this anymore, because rockstargames patched it to be more uniform meaning if we use it this will shutdown the overlay.
                outboundRule.LocalPorts = startport + "-" + endport;
                outboundRule.RemotePorts = startport + "-" + endport;
                */
                inboundRule.LocalPorts = "6672";
                inboundRule.RemotePorts = "6672";
            }
            
            
            INetFwRule2 outboundRule = (INetFwRule2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
            outboundRule.RemoteAddresses = remote;
            outboundRule.LocalAddresses = local;
            outboundRule.Profiles = currentProfiles;
            outboundRule.Protocol = protocol;
            outboundRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;
            outboundRule.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK;
            outboundRule.Name = "GAME_BLOCK";
            outboundRule.Enabled = true;
            if (protocol != FW_BOTH)
            {
                /**
                we do not use this anymore, because rockstargames patched it to be more uniform meaning if we use it this will shutdown the overlay.
                outboundRule.LocalPorts = startport + "-" + endport;
                outboundRule.RemotePorts = startport + "-" + endport;
                */
                inboundRule.LocalPorts = "6672";
                inboundRule.RemotePorts = "6672";
            }
            

            firewallPolicy.Rules.Add(inboundRule);
            firewallPolicy.Rules.Add(outboundRule);

        }

        public void RemoveRules()
        {
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            IEnumerator it = firewallPolicy.Rules.GetEnumerator();

            while (it.MoveNext())
            {
                INetFwRule2 fwr = (INetFwRule2)it.Current;
                if (fwr.Name.ToLower().Equals("GAME_BLOCK".ToLower()))
                {
                    firewallPolicy.Rules.Remove(fwr.Name);
                }
            }
        }

    }
}
