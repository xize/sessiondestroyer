﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace SessionDestroyer
{
    partial class Window : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pic2 = new System.Windows.Forms.Panel();
            this.pic1 = new System.Windows.Forms.Panel();
            this.pic4 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.udp = new System.Windows.Forms.CheckBox();
            this.tcp = new System.Windows.Forms.CheckBox();
            this.targeticon = new System.Windows.Forms.PictureBox();
            this.hook = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.fwignore = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.wifitextbox = new System.Windows.Forms.TextBox();
            this.wificheckbox = new System.Windows.Forms.CheckBox();
            this.lastaction = new System.Windows.Forms.Label();
            this.Rresult = new System.Windows.Forms.Label();
            this.mutebtn = new System.Windows.Forms.CheckBox();
            this.pic3 = new System.Windows.Forms.Panel();
            this.disablenetbtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.killprocessbtn = new System.Windows.Forms.Button();
            this.openbtn = new System.Windows.Forms.Button();
            this.closebtn = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.collapsebtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.localid = new System.Windows.Forms.Label();
            this.minimizebtn2 = new System.Windows.Forms.Panel();
            this.winclosebtn2 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.idlabel = new System.Windows.Forms.Label();
            this.minimizebtn = new System.Windows.Forms.Panel();
            this.winclosebtn = new System.Windows.Forms.Panel();
            this.progress = new SessionDestroyer.controls.CustomProgressbar();
            this.whitelistpage = new System.Windows.Forms.TabControl();
            this.homepage = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.questionicon = new System.Windows.Forms.Panel();
            this.whitelist = new System.Windows.Forms.ListView();
            this.nicknamecolum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.idnumbercollum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.delbtn = new System.Windows.Forms.Button();
            this.nickname = new System.Windows.Forms.TextBox();
            this.id = new System.Windows.Forms.TextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.questionicon2 = new System.Windows.Forms.Panel();
            this.whitelist2 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.delbtn2 = new System.Windows.Forms.Button();
            this.nickname2 = new System.Windows.Forms.TextBox();
            this.id2 = new System.Windows.Forms.TextBox();
            this.addbtn2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.moveableWindow1 = new SessionDestroyer.controls.MoveableWindow();
            this.roundedcornercontrol = new SessionDestroyer.controls.RoundedCornersControl();
            this.roundedCornersControl1 = new SessionDestroyer.controls.RoundedCornersControl();
            this.idrounder = new SessionDestroyer.controls.RoundedCornersControl();
            this.addbtnround = new SessionDestroyer.controls.RoundedCornersControl();
            this.delbtnround = new SessionDestroyer.controls.RoundedCornersControl();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextopensessionbtn = new System.Windows.Forms.ToolStripMenuItem();
            this.contextclosesessionbtn = new System.Windows.Forms.ToolStripMenuItem();
            this.contextdisablenetbtn = new System.Windows.Forms.ToolStripMenuItem();
            this.contextcloseprocbtn = new System.Windows.Forms.ToolStripMenuItem();
            this.contextclosebtn = new System.Windows.Forms.ToolStripMenuItem();
            this.roundedCornersControl2 = new SessionDestroyer.controls.RoundedCornersControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.targeticon)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.whitelistpage.SuspendLayout();
            this.homepage.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_work);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.worker_completed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Yu Gothic UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(53, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "SessionDestroyer";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SessionDestroyer.Properties.Resources.disconnected_50px;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Location = new System.Drawing.Point(205, 232);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(51, 50);
            this.panel7.TabIndex = 4;
            this.panel7.MouseEnter += new System.EventHandler(this.panel4_MouseEnter);
            this.panel7.MouseLeave += new System.EventHandler(this.panel7_MouseLeave);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::SessionDestroyer.Properties.Resources.available_updates_64px;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel4.Location = new System.Drawing.Point(13, 11);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(10);
            this.panel4.Size = new System.Drawing.Size(26, 28);
            this.panel4.TabIndex = 3;
            this.panel4.Click += new System.EventHandler(this.panel4_Click);
            this.panel4.MouseEnter += new System.EventHandler(this.panel4_MouseEnter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.LightGray;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 120);
            this.label4.TabIndex = 5;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(5, 365);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label3.Size = new System.Drawing.Size(30, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Active";
            // 
            // pic2
            // 
            this.pic2.BackColor = System.Drawing.Color.Transparent;
            this.pic2.BackgroundImage = global::SessionDestroyer.Properties.Resources.open_view_24px;
            this.pic2.Location = new System.Drawing.Point(178, 149);
            this.pic2.Name = "pic2";
            this.pic2.Padding = new System.Windows.Forms.Padding(6);
            this.pic2.Size = new System.Drawing.Size(24, 24);
            this.pic2.TabIndex = 10;
            // 
            // pic1
            // 
            this.pic1.BackColor = System.Drawing.Color.Transparent;
            this.pic1.BackgroundImage = global::SessionDestroyer.Properties.Resources.close24px_btn;
            this.pic1.Location = new System.Drawing.Point(178, 106);
            this.pic1.Name = "pic1";
            this.pic1.Padding = new System.Windows.Forms.Padding(6);
            this.pic1.Size = new System.Drawing.Size(24, 24);
            this.pic1.TabIndex = 9;
            // 
            // pic4
            // 
            this.pic4.BackColor = System.Drawing.Color.Transparent;
            this.pic4.BackgroundImage = global::SessionDestroyer.Properties.Resources.closepane;
            this.pic4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic4.Location = new System.Drawing.Point(178, 237);
            this.pic4.Name = "pic4";
            this.pic4.Padding = new System.Windows.Forms.Padding(6);
            this.pic4.Size = new System.Drawing.Size(24, 24);
            this.pic4.TabIndex = 11;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel9.Controls.Add(this.udp);
            this.panel9.Controls.Add(this.tcp);
            this.panel9.Controls.Add(this.targeticon);
            this.panel9.Controls.Add(this.hook);
            this.panel9.Location = new System.Drawing.Point(3, 275);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(209, 28);
            this.panel9.TabIndex = 14;
            // 
            // udp
            // 
            this.udp.AutoSize = true;
            this.udp.Checked = true;
            this.udp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.udp.Dock = System.Windows.Forms.DockStyle.Left;
            this.udp.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.udp.ForeColor = System.Drawing.Color.LightGray;
            this.udp.Location = new System.Drawing.Point(53, 0);
            this.udp.Name = "udp";
            this.udp.Size = new System.Drawing.Size(48, 28);
            this.udp.TabIndex = 3;
            this.udp.Text = "UDP";
            this.udp.UseVisualStyleBackColor = true;
            // 
            // tcp
            // 
            this.tcp.AutoSize = true;
            this.tcp.Dock = System.Windows.Forms.DockStyle.Left;
            this.tcp.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.tcp.ForeColor = System.Drawing.Color.LightGray;
            this.tcp.Location = new System.Drawing.Point(0, 0);
            this.tcp.Name = "tcp";
            this.tcp.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.tcp.Size = new System.Drawing.Size(53, 28);
            this.tcp.TabIndex = 2;
            this.tcp.Text = "TCP";
            this.tcp.UseVisualStyleBackColor = true;
            // 
            // targeticon
            // 
            this.targeticon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.targeticon.BackgroundImage = global::SessionDestroyer.Properties.Resources.window_target1;
            this.targeticon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.targeticon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.targeticon.Location = new System.Drawing.Point(186, 5);
            this.targeticon.Name = "targeticon";
            this.targeticon.Size = new System.Drawing.Size(20, 20);
            this.targeticon.TabIndex = 11;
            this.targeticon.TabStop = false;
            this.targeticon.Click += new System.EventHandler(this.targeticon_Click);
            this.targeticon.MouseEnter += new System.EventHandler(this.targetover);
            this.targeticon.MouseLeave += new System.EventHandler(this.targetleave);
            // 
            // hook
            // 
            this.hook.AutoEllipsis = true;
            this.hook.BackColor = System.Drawing.Color.Transparent;
            this.hook.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.hook.ForeColor = System.Drawing.Color.LightGray;
            this.hook.Location = new System.Drawing.Point(103, 7);
            this.hook.Name = "hook";
            this.hook.Size = new System.Drawing.Size(71, 13);
            this.hook.TabIndex = 12;
            this.hook.Text = "process drag:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.fwignore);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.wifitextbox);
            this.panel1.Controls.Add(this.wificheckbox);
            this.panel1.Controls.Add(this.lastaction);
            this.panel1.Controls.Add(this.Rresult);
            this.panel1.Controls.Add(this.mutebtn);
            this.panel1.Controls.Add(this.pic3);
            this.panel1.Controls.Add(this.disablenetbtn);
            this.panel1.Controls.Add(this.pic4);
            this.panel1.Controls.Add(this.pic2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.killprocessbtn);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.openbtn);
            this.panel1.Controls.Add(this.pic1);
            this.panel1.Controls.Add(this.closebtn);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.progress);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(502, 443);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // fwignore
            // 
            this.fwignore.AutoSize = true;
            this.fwignore.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.fwignore.ForeColor = System.Drawing.Color.LightGray;
            this.fwignore.Location = new System.Drawing.Point(3, 347);
            this.fwignore.Name = "fwignore";
            this.fwignore.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.fwignore.Size = new System.Drawing.Size(188, 17);
            this.fwignore.TabIndex = 23;
            this.fwignore.Text = "do not set firewall only suspend";
            this.fwignore.UseVisualStyleBackColor = true;
            this.fwignore.CheckStateChanged += new System.EventHandler(this.fwignore_CheckStateChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MidnightBlue;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(149, 322);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.button1.Size = new System.Drawing.Size(53, 22);
            this.button1.TabIndex = 22;
            this.button1.Text = "profiles";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // wifitextbox
            // 
            this.wifitextbox.BackColor = System.Drawing.Color.Black;
            this.wifitextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wifitextbox.Enabled = false;
            this.wifitextbox.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.wifitextbox.ForeColor = System.Drawing.Color.LightGray;
            this.wifitextbox.Location = new System.Drawing.Point(56, 322);
            this.wifitextbox.Name = "wifitextbox";
            this.wifitextbox.Size = new System.Drawing.Size(87, 22);
            this.wifitextbox.TabIndex = 21;
            this.wifitextbox.Text = "wifi profile";
            this.wifitextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.wifitextbox.Click += new System.EventHandler(this.wifitextbox_Click);
            // 
            // wificheckbox
            // 
            this.wificheckbox.AutoSize = true;
            this.wificheckbox.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.wificheckbox.ForeColor = System.Drawing.Color.LightGray;
            this.wificheckbox.Location = new System.Drawing.Point(3, 324);
            this.wificheckbox.Name = "wificheckbox";
            this.wificheckbox.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.wificheckbox.Size = new System.Drawing.Size(55, 17);
            this.wificheckbox.TabIndex = 20;
            this.wificheckbox.Text = "Wifi:";
            this.wificheckbox.UseVisualStyleBackColor = true;
            this.wificheckbox.CheckedChanged += new System.EventHandler(this.wificheckbox_CheckedChanged);
            // 
            // lastaction
            // 
            this.lastaction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lastaction.AutoEllipsis = true;
            this.lastaction.BackColor = System.Drawing.Color.Transparent;
            this.lastaction.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.lastaction.ForeColor = System.Drawing.Color.Gray;
            this.lastaction.Location = new System.Drawing.Point(54, 423);
            this.lastaction.Name = "lastaction";
            this.lastaction.Size = new System.Drawing.Size(148, 20);
            this.lastaction.TabIndex = 19;
            this.lastaction.Text = "did nothing yet.";
            // 
            // Rresult
            // 
            this.Rresult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Rresult.AutoSize = true;
            this.Rresult.BackColor = System.Drawing.Color.Transparent;
            this.Rresult.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.Rresult.ForeColor = System.Drawing.Color.Gray;
            this.Rresult.Location = new System.Drawing.Point(5, 423);
            this.Rresult.Name = "Rresult";
            this.Rresult.Size = new System.Drawing.Size(49, 12);
            this.Rresult.TabIndex = 18;
            this.Rresult.Text = "Last action:";
            // 
            // mutebtn
            // 
            this.mutebtn.AutoSize = true;
            this.mutebtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.mutebtn.ForeColor = System.Drawing.Color.LightGray;
            this.mutebtn.Location = new System.Drawing.Point(3, 301);
            this.mutebtn.Name = "mutebtn";
            this.mutebtn.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.mutebtn.Size = new System.Drawing.Size(80, 17);
            this.mutebtn.TabIndex = 13;
            this.mutebtn.Text = "Sound off";
            this.mutebtn.UseVisualStyleBackColor = true;
            // 
            // pic3
            // 
            this.pic3.BackColor = System.Drawing.Color.Transparent;
            this.pic3.BackgroundImage = global::SessionDestroyer.Properties.Resources.no_network_24px;
            this.pic3.Location = new System.Drawing.Point(178, 194);
            this.pic3.Name = "pic3";
            this.pic3.Padding = new System.Windows.Forms.Padding(6);
            this.pic3.Size = new System.Drawing.Size(24, 24);
            this.pic3.TabIndex = 11;
            // 
            // disablenetbtn
            // 
            this.disablenetbtn.BackColor = System.Drawing.Color.Black;
            this.disablenetbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.disablenetbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.disablenetbtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.disablenetbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.disablenetbtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disablenetbtn.ForeColor = System.Drawing.Color.White;
            this.disablenetbtn.Location = new System.Drawing.Point(0, 186);
            this.disablenetbtn.Name = "disablenetbtn";
            this.disablenetbtn.Size = new System.Drawing.Size(212, 38);
            this.disablenetbtn.TabIndex = 17;
            this.disablenetbtn.Text = "F3: disable network";
            this.disablenetbtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.disablenetbtn.UseVisualStyleBackColor = false;
            this.disablenetbtn.Click += new System.EventHandler(this.OnDisablenetwork);
            this.disablenetbtn.MouseEnter += new System.EventHandler(this.OnButtonEnterEvent);
            this.disablenetbtn.MouseLeave += new System.EventHandler(this.OnButtonLeaveEvent);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label7.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Underline);
            this.label7.ForeColor = System.Drawing.Color.LightGray;
            this.label7.Location = new System.Drawing.Point(131, 372);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "save settings";
            this.label7.Click += new System.EventHandler(this.saveclickevent);
            // 
            // killprocessbtn
            // 
            this.killprocessbtn.BackColor = System.Drawing.Color.Black;
            this.killprocessbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.killprocessbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.killprocessbtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.killprocessbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.killprocessbtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.killprocessbtn.ForeColor = System.Drawing.Color.White;
            this.killprocessbtn.Location = new System.Drawing.Point(0, 230);
            this.killprocessbtn.Name = "killprocessbtn";
            this.killprocessbtn.Size = new System.Drawing.Size(212, 38);
            this.killprocessbtn.TabIndex = 13;
            this.killprocessbtn.Text = "F4: Close process";
            this.killprocessbtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.killprocessbtn.UseVisualStyleBackColor = false;
            this.killprocessbtn.Click += new System.EventHandler(this.button1_Click_1);
            this.killprocessbtn.MouseEnter += new System.EventHandler(this.OnButtonEnterEvent);
            this.killprocessbtn.MouseLeave += new System.EventHandler(this.OnButtonLeaveEvent);
            // 
            // openbtn
            // 
            this.openbtn.BackColor = System.Drawing.Color.Black;
            this.openbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.openbtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.openbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openbtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openbtn.ForeColor = System.Drawing.Color.White;
            this.openbtn.Location = new System.Drawing.Point(0, 142);
            this.openbtn.Name = "openbtn";
            this.openbtn.Size = new System.Drawing.Size(212, 38);
            this.openbtn.TabIndex = 4;
            this.openbtn.Text = "F2: Open session";
            this.openbtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.openbtn.UseVisualStyleBackColor = false;
            this.openbtn.Click += new System.EventHandler(this.button2_Click);
            this.openbtn.MouseEnter += new System.EventHandler(this.OnButtonEnterEvent);
            this.openbtn.MouseLeave += new System.EventHandler(this.OnButtonLeaveEvent);
            // 
            // closebtn
            // 
            this.closebtn.BackColor = System.Drawing.Color.Black;
            this.closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closebtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.closebtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.closebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closebtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closebtn.ForeColor = System.Drawing.Color.White;
            this.closebtn.Location = new System.Drawing.Point(0, 100);
            this.closebtn.Name = "closebtn";
            this.closebtn.Size = new System.Drawing.Size(212, 38);
            this.closebtn.TabIndex = 3;
            this.closebtn.Text = "F1: Close Session";
            this.closebtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.closebtn.UseVisualStyleBackColor = false;
            this.closebtn.Click += new System.EventHandler(this.button1_Click);
            this.closebtn.MouseEnter += new System.EventHandler(this.OnButtonEnterEvent);
            this.closebtn.MouseLeave += new System.EventHandler(this.OnButtonLeaveEvent);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.Controls.Add(this.collapsebtn);
            this.panel10.Controls.Add(this.panel2);
            this.panel10.Controls.Add(this.minimizebtn);
            this.panel10.Controls.Add(this.winclosebtn);
            this.panel10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(502, 100);
            this.panel10.TabIndex = 12;
            // 
            // collapsebtn
            // 
            this.collapsebtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.collapsebtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.collapsebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.collapsebtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.collapsebtn.ForeColor = System.Drawing.Color.White;
            this.collapsebtn.Location = new System.Drawing.Point(192, 40);
            this.collapsebtn.Name = "collapsebtn";
            this.collapsebtn.Size = new System.Drawing.Size(17, 38);
            this.collapsebtn.TabIndex = 2;
            this.collapsebtn.Text = ">";
            this.toolTip1.SetToolTip(this.collapsebtn, "toggle window collapse");
            this.collapsebtn.UseVisualStyleBackColor = false;
            this.collapsebtn.Click += new System.EventHandler(this.collapsebtn_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.minimizebtn2);
            this.panel2.Controls.Add(this.winclosebtn2);
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(279, 54);
            this.panel2.TabIndex = 1;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel11.Controls.Add(this.localid);
            this.panel11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel11.Location = new System.Drawing.Point(59, 31);
            this.panel11.Name = "panel11";
            this.panel11.Padding = new System.Windows.Forms.Padding(5);
            this.panel11.Size = new System.Drawing.Size(157, 23);
            this.panel11.TabIndex = 11;
            this.panel11.Click += new System.EventHandler(this.oncopylocalid);
            // 
            // localid
            // 
            this.localid.AutoEllipsis = true;
            this.localid.BackColor = System.Drawing.Color.Transparent;
            this.localid.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.localid.ForeColor = System.Drawing.Color.LightGray;
            this.localid.Location = new System.Drawing.Point(8, 4);
            this.localid.Name = "localid";
            this.localid.Size = new System.Drawing.Size(141, 12);
            this.localid.TabIndex = 9;
            this.localid.Text = "Your local ID: {0}";
            this.localid.Click += new System.EventHandler(this.oncopylocalid);
            // 
            // minimizebtn2
            // 
            this.minimizebtn2.BackgroundImage = global::SessionDestroyer.Properties.Resources.minimizebtn;
            this.minimizebtn2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.minimizebtn2.Location = new System.Drawing.Point(224, 14);
            this.minimizebtn2.Name = "minimizebtn2";
            this.minimizebtn2.Size = new System.Drawing.Size(19, 16);
            this.minimizebtn2.TabIndex = 9;
            this.minimizebtn2.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // winclosebtn2
            // 
            this.winclosebtn2.BackgroundImage = global::SessionDestroyer.Properties.Resources.closebtn1;
            this.winclosebtn2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.winclosebtn2.Location = new System.Drawing.Point(249, 8);
            this.winclosebtn2.Name = "winclosebtn2";
            this.winclosebtn2.Size = new System.Drawing.Size(19, 22);
            this.winclosebtn2.TabIndex = 10;
            this.winclosebtn2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel12.Controls.Add(this.idlabel);
            this.panel12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel12.Location = new System.Drawing.Point(59, 7);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(5);
            this.panel12.Size = new System.Drawing.Size(157, 23);
            this.panel12.TabIndex = 10;
            this.panel12.Click += new System.EventHandler(this.idlabel_Click);
            // 
            // idlabel
            // 
            this.idlabel.AutoEllipsis = true;
            this.idlabel.BackColor = System.Drawing.Color.Transparent;
            this.idlabel.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.idlabel.ForeColor = System.Drawing.Color.LightGray;
            this.idlabel.Location = new System.Drawing.Point(8, 4);
            this.idlabel.Name = "idlabel";
            this.idlabel.Size = new System.Drawing.Size(141, 12);
            this.idlabel.TabIndex = 9;
            this.idlabel.Text = "Your ID: {0}";
            this.idlabel.Click += new System.EventHandler(this.idlabel_Click);
            // 
            // minimizebtn
            // 
            this.minimizebtn.BackgroundImage = global::SessionDestroyer.Properties.Resources.minimizebtn;
            this.minimizebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.minimizebtn.Location = new System.Drawing.Point(169, 8);
            this.minimizebtn.Name = "minimizebtn";
            this.minimizebtn.Size = new System.Drawing.Size(19, 16);
            this.minimizebtn.TabIndex = 7;
            this.minimizebtn.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // winclosebtn
            // 
            this.winclosebtn.BackgroundImage = global::SessionDestroyer.Properties.Resources.closebtn1;
            this.winclosebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.winclosebtn.Location = new System.Drawing.Point(190, 3);
            this.winclosebtn.Name = "winclosebtn";
            this.winclosebtn.Size = new System.Drawing.Size(19, 22);
            this.winclosebtn.TabIndex = 8;
            this.winclosebtn.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // progress
            // 
            this.progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progress.BackColor = System.Drawing.Color.Black;
            this.progress.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progress.CornerRadius = 8;
            this.progress.Location = new System.Drawing.Point(7, 391);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(202, 25);
            this.progress.TabIndex = 15;
            // 
            // whitelistpage
            // 
            this.whitelistpage.Controls.Add(this.homepage);
            this.whitelistpage.Controls.Add(this.tabPage2);
            this.whitelistpage.Controls.Add(this.tabPage3);
            this.whitelistpage.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.whitelistpage.Location = new System.Drawing.Point(218, 74);
            this.whitelistpage.Name = "whitelistpage";
            this.whitelistpage.SelectedIndex = 0;
            this.whitelistpage.Size = new System.Drawing.Size(270, 314);
            this.whitelistpage.TabIndex = 1;
            // 
            // homepage
            // 
            this.homepage.BackColor = System.Drawing.Color.Black;
            this.homepage.Controls.Add(this.panel7);
            this.homepage.Controls.Add(this.label4);
            this.homepage.Location = new System.Drawing.Point(4, 21);
            this.homepage.Name = "homepage";
            this.homepage.Padding = new System.Windows.Forms.Padding(3);
            this.homepage.Size = new System.Drawing.Size(262, 289);
            this.homepage.TabIndex = 0;
            this.homepage.Text = "Home";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Black;
            this.tabPage2.Controls.Add(this.questionicon);
            this.tabPage2.Controls.Add(this.whitelist);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.delbtn);
            this.tabPage2.Controls.Add(this.nickname);
            this.tabPage2.Controls.Add(this.id);
            this.tabPage2.Controls.Add(this.addbtn);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(262, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Whitelist [Remote]";
            // 
            // questionicon
            // 
            this.questionicon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.questionicon.Location = new System.Drawing.Point(219, 6);
            this.questionicon.Name = "questionicon";
            this.questionicon.Size = new System.Drawing.Size(30, 32);
            this.questionicon.TabIndex = 14;
            // 
            // whitelist
            // 
            this.whitelist.BackColor = System.Drawing.Color.Black;
            this.whitelist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nicknamecolum,
            this.idnumbercollum});
            this.whitelist.Dock = System.Windows.Forms.DockStyle.Top;
            this.whitelist.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.whitelist.ForeColor = System.Drawing.Color.LightGray;
            this.whitelist.FullRowSelect = true;
            this.whitelist.HideSelection = false;
            this.whitelist.LabelEdit = true;
            this.whitelist.Location = new System.Drawing.Point(3, 44);
            this.whitelist.Name = "whitelist";
            this.whitelist.Size = new System.Drawing.Size(256, 179);
            this.whitelist.TabIndex = 12;
            this.whitelist.UseCompatibleStateImageBehavior = false;
            this.whitelist.View = System.Windows.Forms.View.Details;
            this.whitelist.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.whitelist_DrawItem);
            this.whitelist.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.whitelist_ItemSelectionChanged);
            this.whitelist.VisibleChanged += new System.EventHandler(this.whitelist_VisibleChanged);
            // 
            // nicknamecolum
            // 
            this.nicknamecolum.Text = "Nickname";
            this.nicknamecolum.Width = 144;
            // 
            // idnumbercollum
            // 
            this.idnumbercollum.Text = "identiy";
            this.idnumbercollum.Width = 80;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.LightGray;
            this.label5.Location = new System.Drawing.Point(83, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.LightGray;
            this.label2.Location = new System.Drawing.Point(4, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "nickname:";
            // 
            // delbtn
            // 
            this.delbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.delbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.delbtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(160)))));
            this.delbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delbtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.delbtn.ForeColor = System.Drawing.Color.White;
            this.delbtn.Location = new System.Drawing.Point(207, 247);
            this.delbtn.Name = "delbtn";
            this.delbtn.Size = new System.Drawing.Size(37, 23);
            this.delbtn.TabIndex = 5;
            this.delbtn.Text = "del";
            this.delbtn.UseVisualStyleBackColor = false;
            this.delbtn.Click += new System.EventHandler(this.delbtn_Click);
            // 
            // nickname
            // 
            this.nickname.Location = new System.Drawing.Point(6, 249);
            this.nickname.Name = "nickname";
            this.nickname.Size = new System.Drawing.Size(73, 19);
            this.nickname.TabIndex = 2;
            this.nickname.Text = "<nickname>";
            this.nickname.Click += new System.EventHandler(this.nickname_Click);
            this.nickname.TextChanged += new System.EventHandler(this.nickname_TextChanged);
            // 
            // id
            // 
            this.id.Location = new System.Drawing.Point(85, 249);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(73, 19);
            this.id.TabIndex = 3;
            this.id.Text = "<id>";
            this.id.Click += new System.EventHandler(this.idclick);
            this.id.TextChanged += new System.EventHandler(this.idchangeevent);
            // 
            // addbtn
            // 
            this.addbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.addbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.addbtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(160)))));
            this.addbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addbtn.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.addbtn.ForeColor = System.Drawing.Color.White;
            this.addbtn.Location = new System.Drawing.Point(164, 247);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(37, 23);
            this.addbtn.TabIndex = 4;
            this.addbtn.Text = "add";
            this.addbtn.UseVisualStyleBackColor = false;
            this.addbtn.Click += new System.EventHandler(this.addbtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.LightGray;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label6.Size = new System.Drawing.Size(210, 41);
            this.label6.TabIndex = 13;
            this.label6.Text = "note: in some games whitelist does not work this-\r\nbecause the game abuses the ot" +
    "her clients to enter-\r\nyour game, identities are bound to ip.";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Black;
            this.tabPage3.Controls.Add(this.questionicon2);
            this.tabPage3.Controls.Add(this.whitelist2);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.delbtn2);
            this.tabPage3.Controls.Add(this.nickname2);
            this.tabPage3.Controls.Add(this.id2);
            this.tabPage3.Controls.Add(this.addbtn2);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Location = new System.Drawing.Point(4, 21);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(262, 289);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Whitelist [Local]";
            // 
            // questionicon2
            // 
            this.questionicon2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.questionicon2.Location = new System.Drawing.Point(219, 7);
            this.questionicon2.Name = "questionicon2";
            this.questionicon2.Size = new System.Drawing.Size(30, 32);
            this.questionicon2.TabIndex = 23;
            // 
            // whitelist2
            // 
            this.whitelist2.BackColor = System.Drawing.Color.Black;
            this.whitelist2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.whitelist2.Dock = System.Windows.Forms.DockStyle.Top;
            this.whitelist2.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.whitelist2.ForeColor = System.Drawing.Color.LightGray;
            this.whitelist2.FullRowSelect = true;
            this.whitelist2.HideSelection = false;
            this.whitelist2.LabelEdit = true;
            this.whitelist2.Location = new System.Drawing.Point(3, 44);
            this.whitelist2.Name = "whitelist2";
            this.whitelist2.Size = new System.Drawing.Size(256, 179);
            this.whitelist2.TabIndex = 21;
            this.whitelist2.UseCompatibleStateImageBehavior = false;
            this.whitelist2.View = System.Windows.Forms.View.Details;
            this.whitelist2.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.whitelist_DrawItem);
            this.whitelist2.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.whitelist_ItemSelectionChanged);
            this.whitelist2.VisibleChanged += new System.EventHandler(this.whitelist_VisibleChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nickname";
            this.columnHeader3.Width = 144;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "identiy";
            this.columnHeader4.Width = 80;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.LightGray;
            this.label9.Location = new System.Drawing.Point(83, 235);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 12);
            this.label9.TabIndex = 20;
            this.label9.Text = "id:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.LightGray;
            this.label10.Location = new System.Drawing.Point(4, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 12);
            this.label10.TabIndex = 19;
            this.label10.Text = "nickname:";
            // 
            // delbtn2
            // 
            this.delbtn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.delbtn2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.delbtn2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(160)))));
            this.delbtn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delbtn2.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.delbtn2.ForeColor = System.Drawing.Color.White;
            this.delbtn2.Location = new System.Drawing.Point(207, 247);
            this.delbtn2.Name = "delbtn2";
            this.delbtn2.Size = new System.Drawing.Size(37, 23);
            this.delbtn2.TabIndex = 18;
            this.delbtn2.Text = "del";
            this.delbtn2.UseVisualStyleBackColor = false;
            this.delbtn2.Click += new System.EventHandler(this.delbtn2_Click);
            // 
            // nickname2
            // 
            this.nickname2.Location = new System.Drawing.Point(6, 249);
            this.nickname2.Name = "nickname2";
            this.nickname2.Size = new System.Drawing.Size(73, 19);
            this.nickname2.TabIndex = 15;
            this.nickname2.Text = "<nickname>";
            this.nickname2.Click += new System.EventHandler(this.nickname2_Click);
            this.nickname2.TextChanged += new System.EventHandler(this.nickname2_TextChanged);
            // 
            // id2
            // 
            this.id2.Location = new System.Drawing.Point(85, 249);
            this.id2.Name = "id2";
            this.id2.Size = new System.Drawing.Size(73, 19);
            this.id2.TabIndex = 16;
            this.id2.Text = "<local id>";
            this.id2.Click += new System.EventHandler(this.id2click);
            this.id2.TextChanged += new System.EventHandler(this.id2changeevent);
            // 
            // addbtn2
            // 
            this.addbtn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.addbtn2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.addbtn2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(160)))));
            this.addbtn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addbtn2.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.addbtn2.ForeColor = System.Drawing.Color.White;
            this.addbtn2.Location = new System.Drawing.Point(164, 247);
            this.addbtn2.Name = "addbtn2";
            this.addbtn2.Size = new System.Drawing.Size(37, 23);
            this.addbtn2.TabIndex = 17;
            this.addbtn2.Text = "add";
            this.addbtn2.UseVisualStyleBackColor = false;
            this.addbtn2.Click += new System.EventHandler(this.addbtn2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Yu Gothic UI", 6.75F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.LightGray;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label11.Size = new System.Drawing.Size(210, 41);
            this.label11.TabIndex = 22;
            this.label11.Text = "note: in some games whitelist does not work this-\r\nbecause the game abuses the ot" +
    "her clients to enter-\r\nyour game, identities are bound to ip.";
            // 
            // moveableWindow1
            // 
            this.moveableWindow1.ControlTargets = new System.Windows.Forms.Control[0];
            this.moveableWindow1.MainWindowTarget = this;
            // 
            // roundedcornercontrol
            // 
            this.roundedcornercontrol.Radius = 9;
            this.roundedcornercontrol.TargetControl = this;
            // 
            // roundedCornersControl1
            // 
            this.roundedCornersControl1.Radius = 8;
            this.roundedCornersControl1.TargetControl = this.panel7;
            // 
            // idrounder
            // 
            this.idrounder.Radius = 8;
            this.idrounder.TargetControl = this.panel12;
            // 
            // addbtnround
            // 
            this.addbtnround.Radius = 8;
            this.addbtnround.TargetControl = this.addbtn;
            // 
            // delbtnround
            // 
            this.delbtnround.Radius = 8;
            this.delbtnround.TargetControl = this.delbtn;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipTitle = "SessionDestroyer";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "SessionDestroyer";
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextopensessionbtn,
            this.contextclosesessionbtn,
            this.contextdisablenetbtn,
            this.contextcloseprocbtn,
            this.contextclosebtn});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 114);
            this.contextMenuStrip1.Text = "SessionDestroyer";
            // 
            // contextopensessionbtn
            // 
            this.contextopensessionbtn.Name = "contextopensessionbtn";
            this.contextopensessionbtn.Size = new System.Drawing.Size(157, 22);
            this.contextopensessionbtn.Text = "open session";
            this.contextopensessionbtn.Click += new System.EventHandler(this.contextopensessionbtn_Click);
            // 
            // contextclosesessionbtn
            // 
            this.contextclosesessionbtn.Name = "contextclosesessionbtn";
            this.contextclosesessionbtn.Size = new System.Drawing.Size(157, 22);
            this.contextclosesessionbtn.Text = "close session";
            this.contextclosesessionbtn.Click += new System.EventHandler(this.contextclosesessionbtn_Click);
            // 
            // contextdisablenetbtn
            // 
            this.contextdisablenetbtn.Name = "contextdisablenetbtn";
            this.contextdisablenetbtn.Size = new System.Drawing.Size(157, 22);
            this.contextdisablenetbtn.Text = "disable network";
            this.contextdisablenetbtn.Click += new System.EventHandler(this.contextdisablenetbtn_Click);
            // 
            // contextcloseprocbtn
            // 
            this.contextcloseprocbtn.Name = "contextcloseprocbtn";
            this.contextcloseprocbtn.Size = new System.Drawing.Size(157, 22);
            this.contextcloseprocbtn.Text = "close process";
            this.contextcloseprocbtn.Click += new System.EventHandler(this.contextcloseprocbtn_Click);
            // 
            // contextclosebtn
            // 
            this.contextclosebtn.Name = "contextclosebtn";
            this.contextclosebtn.Size = new System.Drawing.Size(157, 22);
            this.contextclosebtn.Text = "Close";
            this.contextclosebtn.Click += new System.EventHandler(this.contextclosebtn_Click);
            // 
            // roundedCornersControl2
            // 
            this.roundedCornersControl2.Radius = 8;
            this.roundedCornersControl2.TargetControl = this.button1;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::SessionDestroyer.Properties.Resources.layout2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(214, 443);
            this.Controls.Add(this.whitelistpage);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(214, 443);
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SessionDestroyer";
            this.HotKeyPressEvent += new System.EventHandler<SessionDestroyer.util.HotkeyArguments>(this.hotkeyf4press);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.onclosefixcursor);
            this.Load += new System.EventHandler(this.Window_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.targeticon)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.whitelistpage.ResumeLayout(false);
            this.homepage.ResumeLayout(false);
            this.homepage.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.ComponentModel.BackgroundWorker worker;
        private controls.MoveableWindow moveableWindow1;
        private controls.RoundedCornersControl roundedcornercontrol;
        private controls.RoundedCornersControl roundedCornersControl1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.CheckBox udp;
        private System.Windows.Forms.CheckBox tcp;
        private System.Windows.Forms.PictureBox targeticon;
        private System.Windows.Forms.Label hook;
        private System.Windows.Forms.Panel pic4;
        private System.Windows.Forms.Panel pic1;
        private System.Windows.Forms.Button killprocessbtn;
        private System.Windows.Forms.Panel pic2;
        private System.Windows.Forms.Button openbtn;
        private System.Windows.Forms.Button closebtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel minimizebtn;
        private System.Windows.Forms.Panel winclosebtn;
        private System.Windows.Forms.TabControl whitelistpage;
        private System.Windows.Forms.TabPage homepage;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button addbtn;
        private controls.RoundedCornersControl idrounder;
        private System.Windows.Forms.TextBox id;
        private System.Windows.Forms.TextBox nickname;
        private System.Windows.Forms.Button delbtn;
        private controls.RoundedCornersControl addbtnround;
        private controls.RoundedCornersControl delbtnround;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label idlabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView whitelist;
        private System.Windows.Forms.ColumnHeader nicknamecolum;
        private System.Windows.Forms.ColumnHeader idnumbercollum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel questionicon;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel minimizebtn2;
        private System.Windows.Forms.Panel winclosebtn2;
        private controls.CustomProgressbar progress;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel questionicon2;
        private System.Windows.Forms.ListView whitelist2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button delbtn2;
        private System.Windows.Forms.TextBox nickname2;
        private System.Windows.Forms.TextBox id2;
        private System.Windows.Forms.Button addbtn2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label localid;
        private System.Windows.Forms.Label label7;
        public Button collapsebtn;
        private Button disablenetbtn;
        private NotifyIcon notifyIcon1;
        private Panel pic3;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem contextclosesessionbtn;
        private ToolStripMenuItem contextopensessionbtn;
        private ToolStripMenuItem contextdisablenetbtn;
        private ToolStripMenuItem contextcloseprocbtn;
        private ToolStripMenuItem contextclosebtn;
        private CheckBox mutebtn;
        private Label Rresult;
        private Label lastaction;
        private CheckBox wificheckbox;
        private TextBox wifitextbox;
        private Button button1;
        private controls.RoundedCornersControl roundedCornersControl2;
        private CheckBox fwignore;
    }
}

