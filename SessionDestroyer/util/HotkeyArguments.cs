﻿using System.Windows.Forms;

namespace SessionDestroyer.util
{
    public class HotkeyArguments
    {
        
        public HotkeyArguments(Keys hotkey)
        {
            Hotkey = hotkey;
        }

        public Keys Hotkey
        {
            get;private set;
        }

        public bool IsCancelled
        {
            get
            {
                return this.Cancel;
            }
        }

        public bool Cancel
        {
            get;set;
        }



    }
}