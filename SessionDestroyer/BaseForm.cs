﻿using SessionDestroyer.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer
{
    public partial class BaseForm : HotKeyProvider
    {

        private bool collapsed = false;

        public BaseForm()
        {
            InitializeComponent();
        }


        protected void Synchronized(Action a)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                a();
            }));
        }

        [CategoryAttribute("Appearance")]
        public bool Collapse
        {
            get {
                return this.collapsed;   
            }
            set { 
                if(value != this.collapsed)
                {
                    this.collapsed = value;
                    if(value)
                    {
                        this.Size = new Size(500, 443);
                    } else
                    {
                        this.Size = new Size(214, 443);
                    }
                }
            }
        }

    }
}
