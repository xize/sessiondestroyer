![](https://gitlab.com/uploads/-/system/project/avatar/17088540/icons8_disconnected_32.png?width=64)

# SessionDestroyer

### 1. What is SessionDestroyer?
SessionDestroyer is a program which help to isolate yourself from multiplayer peer to peer games, one of such examples are red dead redemption 2 or gta v.
not everyone wants to play this game with modders.

### 2. How does it work?
Once when you close a session firewall rules get added, these block either tcp or udp only or both from 1000-66535 which is the maximum port length.
You can also select a process by using the target wheel, this allows you to also included process suspension to time out even faster.

Note that F4 only works when the process has been attached.


### 3. Where can I find the latest release?
please visit our latest releases [here](https://gitlab.com/xize/sessiondestroyer/-/tags)

### 4. What are the liberaries or tools being used?
Most of our code is coded by ourselfs and Licensed by GPLv3.

however the setup we use since 1.3.0.8 is powered by ["Microsofts Installer Projects"](https://marketplace.visualstudio.com/items?itemName=VisualStudioClient.MicrosoftVisualStudio2017InstallerProjects) extension, prior we used [Wix Toolset](https://wixtoolset.org/) in combination with [WixSharp](https://github.com/oleg-shilo/wixsharp)
all the licences and copyright are reserved by their respective contributers and developers.