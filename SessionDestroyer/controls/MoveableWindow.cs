﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer.controls
{
    public class MoveableWindow : Component
    {
        private bool isMouseClicked = false;
        private Point lastLocation;
        private Form f;
        private Control[] controls;

        public MoveableWindow()
        {

        }

        public Form MainWindowTarget
        {
            get
            {
                return this.f;
            }
            set
            {
                this.f = value;
                makeMoveAble(this.f);
                ///this.f.VisibleChanged += new EventHandler(onChangeVisibility);
            }
        }

        public Control[] ControlTargets
        {
            get
            {
                return this.controls;
            }
            set
            {
                if(value == null)
                {
                    return;
                }
                this.controls = value;
                foreach(Control cs in this.controls)
                {
                    makeMoveAble(cs);
                    //cs.VisibleChanged += new EventHandler(onChangeVisibility);
                }
            }
        }

        private void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);
        }

        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                f.Location = new Point((f.Location.X - lastLocation.X) + e.X, (f.Location.Y - lastLocation.Y) + e.Y);

                f.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.isMouseClicked = true;
                this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.isMouseClicked = false;
            }
        }

        private void onChangeVisibility(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            this.makeMoveAble(c);
        }
    }
}
