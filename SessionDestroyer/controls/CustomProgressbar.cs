﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer.controls
{
    public class CustomProgressbar : ProgressBar
    {

        private Color bcolor;
        private RoundedCornersControl c = new RoundedCornersControl();

        public CustomProgressbar()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            c.TargetControl = this;
            c.Radius = 3;
        }

        public int CornerRadius
        {
            get
            {
                return c.Radius;
            }
            set
            {
                c.Radius = value;
            }
        }

        public Color BackgroundColor
        {
            get { return bcolor; }
            set { 
                this.bcolor = value;
                this.Update();    
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rec = e.ClipRectangle;

            rec.Width = (int)(rec.Width * ((double)Value / Maximum)) - 4;
            if (ProgressBarRenderer.IsSupported)
                ProgressBarRenderer.DrawHorizontalBar(e.Graphics, e.ClipRectangle);
            rec.Height = rec.Height - 4;

            SolidBrush brush = new SolidBrush(bcolor);
            
            e.Graphics.FillRectangle(brush, 2, 2, rec.Width, rec.Height);
        }

    }
}
