﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer.controls
{
    public class RoundedCornersControl : Component
    {

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        private int radius = 30;
        private Control c;

        public RoundedCornersControl()
        {

        }

        public Control TargetControl
        {
            get { return c; }
            set
            {
                this.c = value;
                this.MakeRoundCorners();
                this.c.VisibleChanged += new EventHandler(onChangevis);
                this.c.SizeChanged += new EventHandler(onChangevis);
                this.c.Invalidated += new InvalidateEventHandler(onChangevis_invalidate);
            }
        }

        public int Radius
        {
            get { return this.radius; }
            set
            {
                this.radius = value;
                this.MakeRoundCorners();
            }
        }

        private void MakeRoundCorners()
        {
            if (c == null)
            {
                return;
            }
            this.c.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, c.Width, c.Height, radius, radius));
        }

        private void onChangevis(object sender, EventArgs e)
        {
            this.MakeRoundCorners();
        }

        private void onChangevis_invalidate(object sender, InvalidateEventArgs e)
        {
            
            this.MakeRoundCorners();
        }

        public void Update()
        {
            this.MakeRoundCorners();
        }

    }
}
