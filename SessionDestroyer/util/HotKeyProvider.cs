﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer.util
{
    /**
     * 
     * this class gets extended on BaseForm and vice versa baseform gets extended by Window to use both events and new properties.
     * 
     */
    public class HotKeyProvider : Form
    {

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        protected SpeechSynthesizer speech = new SpeechSynthesizer();
        public event EventHandler<HotkeyArguments> HotKeyPressEvent;

        public HotKeyProvider()
        {
            this.FormClosing += new FormClosingEventHandler(onCloseHotkey);
        }

        private int _threadsafe_locked = 0;

        /**
         * <summary>This returns the Locked boolean for the current thread, once being set the following occurs in both threads:
         * <br></br>
         * When we set Locked to true on the main thread, on any asynchronous concurent thread Locked will be turn to true.
         * 
         * So when we use the hotkeys... we first turn Locked to true from the synchronous event, but we return it false once the event has been finished.
         * and if this was not called to false in the async event... well the code wouldn't work properly or not even return false because the synchronous event
         * isn't aware when the asynchronous event is done and causes all sort of concurent issues and unexpected behaviour we do not want :)
         * 
         * </summary>
         */
        public bool Locked
        {
            get
            {
                return (Interlocked.CompareExchange(ref _threadsafe_locked, 1, 1) == 1);
            }
            set
            {
                if (value)
                    Interlocked.CompareExchange(ref _threadsafe_locked, 1, 0);
                else
                    Interlocked.CompareExchange(ref _threadsafe_locked, 0, 1);
            }
        }

        public String ErrorKeySpam
        {
            get
            {
                return "error please wait 3 seconds after you used a hot key in session destroyer or it will stackup!";
            }
        }

        /**
        * <summary>the duration of the timeout between key presses in seconds</summary>
        */
        public TimeSpan KeyTimeOut
        {
            get
            {
                return TimeSpan.FromSeconds(3);
            }
        }

        public void Speak(string message, bool ignorecancel = false)
        {
            if (!ignorecancel)
            {
                this.speech.SpeakAsyncCancelAll();
            }
            this.speech.SpeakAsync(message);
        }

        public void RegisterKey(Keys key)
        {
            RegisterHotKey(this.Handle, 0, (int)KeyModifier.None, key.GetHashCode());
        }

        public void UnRegisterAll()
        {
            UnregisterHotKey(this.Handle, 0);
        }

        enum KeyModifier
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }

        private void onCloseHotkey(object sender, FormClosingEventArgs e)
        {
            UnregisterHotKey(this.Handle, 0);
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {

                //note to self: only keys get triggered here who are registered by our own function.

                int id = m.WParam.ToInt32();

                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF); // The key of the hotkey that was pressed.

                if (Locked && key != Keys.S) //to avoid spam we use this lock mechanism, and only if the S key was pressed we ignore the check.
                {
                    this.Speak(ErrorKeySpam);
                    return;
                }

                HotkeyArguments args = new HotkeyArguments(key);
                EventHandler<HotkeyArguments> handler = HotKeyPressEvent;

                if (handler != null)
                {
                    this.Locked = true;

                    FireAsyncEvent(handler, args);

                }
            }
        }

        public async void FireAsyncEvent(EventHandler<HotkeyArguments> handler, HotkeyArguments args)
        {
            await Task.Run(() =>
            {
                handler?.Invoke(this, args);
                this.Locked = false;
            });
        }

    }
}
