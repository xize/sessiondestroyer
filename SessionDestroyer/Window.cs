﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using SessionDestroyer;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Speech.Synthesis;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListView;
using SessionDestroyer.util;
using System.Threading;

namespace SessionDestroyer
{
    public partial class Window : BaseForm
    {
        #region externalfuncs
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowScrollBar(IntPtr hWnd, int wBar, [MarshalAs(UnmanagedType.Bool)] bool bShow);
        int SB_HORZ = 0;
        int SB_VERT = 1;
        int SB_BOTH = 3;

        private void HideHorizontalScrollBar()
        {
            ShowScrollBar(this.whitelist.Handle, SB_HORZ, false);
            ShowScrollBar(this.whitelist2.Handle, SB_HORZ, false);
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        private MyFirewall fw = new MyFirewall();
        private FileConfiguration con = new FileConfiguration();
        private Process suspendprocess;
        private uint currentid = 0;
        private GlobalCursor gc = new GlobalCursor();
        #endregion


        private int destroykey = 0;
        private int cancelsuspend = 0;

        public bool DestroyKey
        {
            get
            {
                return (Interlocked.CompareExchange(ref destroykey, 1, 1) == 1);
            }
            set
            {
                if (value)
                {
                    Interlocked.CompareExchange(ref destroykey, 1, 0);
                }
                else
                {
                    Interlocked.CompareExchange(ref destroykey, 0, 1);
                }
            }
        }

        public bool CancelSuspend
        {
            get
            {
                return (Interlocked.CompareExchange(ref cancelsuspend, 1, 1) == 1);
            }
            set
            {
                if (value)
                {
                    Interlocked.CompareExchange(ref cancelsuspend, 1, 0);
                }
                else
                {
                    Interlocked.CompareExchange(ref cancelsuspend, 0, 1);
                }
            }
        }

        public Window()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            #region formload

            this.moveableWindow1.ControlTargets = new Control[] { this.panel2, this.panel10 };

            this.HotKeyPressEvent += this.hotkeyf1press;
            this.HotKeyPressEvent += this.hotkeyf2press;
            this.HotKeyPressEvent += this.hotkeyf3press;
            this.HotKeyPressEvent += this.hotkeyf4press;
            this.HotKeyPressEvent += this.hotkeySpress; //we do not need to unload, because our RegisterHotkey in HotkeyProvider activates the final event.

            this.RegisterHotKeys();

            DownloadID();

            this.questionicon.BackgroundImage = SystemIcons.Question.ToBitmap();
            this.questionicon2.BackgroundImage = SystemIcons.Question.ToBitmap();
            con.LoadData(this.wificheckbox, this.wifitextbox, this.whitelist, this.whitelist2);

            this.UpdateNotify();
            #endregion
        }


        public async void DownloadID()
        {
            string ip = "null";
            WebClient client = new WebClient();
            try
            {
                ip = await client.DownloadStringTaskAsync("https://ipinfo.io/ip");
            }
            catch (Exception)
            {
                ip = "ERROR";
            }
            client.Dispose();

            ip = ip.TrimEnd();

            String id = IDGenerator.GetFactory().GenerateID(ip);

            Synchronized(delegate
            {
                this.idlabel.Text = String.Format(this.idlabel.Text, id);
                this.localid.Text = String.Format(this.localid.Text, IDGenerator.GetFactory().GenerateLocalID());
            });
        }

        public async void UpdateNotify()
        {
            await Task.Run(async () =>
            {
                if (fw.HasRules())
                {
                    Synchronized(delegate
                    {
                        this.notifyIcon1.Visible = true;
                        this.Icon = Properties.Resources.icon_closed;
                        this.notifyIcon1.Icon = this.Icon;
                        this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: session locked.";
                        this.notifyIcon1.BalloonTipText = "sessiondestroyer started with the session closed saved from the last time!";
                        this.notifyIcon1.ShowBalloonTip(3000);
                    });

                    await Task.Delay(3000); //ive read this is bad, but this works fine.

                    Synchronized(delegate
                    {
                        this.notifyIcon1.BalloonTipText = "you can open the window here in the system tray.";
                        this.notifyIcon1.Visible = false;
                        this.progress.Value = 100;
                    });
                }
            });
        }

        public int CDebug = 0;

        public bool Debug
        {
            get
            {
                return Interlocked.CompareExchange(ref CDebug, 1, 1) == 1;
            }
            set
            {
                if (value)
                {
                    Interlocked.CompareExchange(ref CDebug, 1, 0);
                }
                else
                {
                    Interlocked.CompareExchange(ref CDebug, 0, 1);
                }
            }
        }

        #region hotkeys

        public void RegisterHotKeys()
        {
            this.RegisterKey(Keys.F1);
            this.RegisterKey(Keys.F2);
            this.RegisterKey(Keys.F3);
            this.RegisterKey(Keys.F4);
        }

        public void UnRegisterHotKeys()
        {
            this.UnRegisterAll();
        }

        #region hotkeyevents

        private async void hotkeyf1press(object sender, HotkeyArguments e)
        {
            if (e.Hotkey == Keys.F1)
            {
                if (!this.mutebtn.Checked)
                    Console.Beep();

                this.DestroyKey = true;

                Speak("destroying session!");

                if (this.closebtn.Visible && !this.Disposing)
                {
                    if (this.suspendprocess != null && !this.suspendprocess.HasExited)
                    {
                        this.DoSuspend();
                    }

                    this.ToggleFirewall();
                }
                else
                {
                    if (this.suspendprocess != null && !this.suspendprocess.HasExited)
                    {
                        this.DoSuspend();
                    }

                    this.ToggleFirewall();
                }
                await Task.Delay(KeyTimeOut);
                //Thread.Sleep(KeyTimeOut);
            }
        }

        private async void hotkeyf2press(object sender, HotkeyArguments e)
        {
            if (e.Hotkey == Keys.F2)
            {
                if (!this.mutebtn.Checked)
                    Console.Beep();

                Speak("opening session!");

                if (this.openbtn.Visible && !this.openbtn.Disposing)
                    CleanFirewall();
                else
                    CleanFirewall();

            }

            await Task.Delay(KeyTimeOut);
        }

        private async void hotkeyf3press(object sender, HotkeyArguments e)
        {
            if (e.Hotkey == Keys.F3)
            {
                if (!this.mutebtn.Checked)
                    Console.Beep();

                if (this.disablenetbtn.Visible && !this.disablenetbtn.Disposing)
                    ToggleNetwork();
                else
                    ToggleNetwork();

                await Task.Delay(KeyTimeOut);
            }
        }

        private async void hotkeyf4press(object sender, HotkeyArguments e)
        {
            if (e.Hotkey == Keys.F4)
            {
                if (this.suspendprocess != null && !this.suspendprocess.HasExited)
                {
                    if (!this.mutebtn.Checked)
                        Console.Beep();

                    Speak("destroying game process!");

                    if (this.killprocessbtn.Visible && !this.killprocessbtn.Disposing)
                        CloseSuspendProcess();
                    else
                        CloseSuspendProcess();
                }
                else
                {
                    if (!this.mutebtn.Checked)
                        Console.Beep(800, 900);

                    Speak("no attached process");
                }

                await Task.Delay(KeyTimeOut);
            }
        }

        private void hotkeySpress(object sender, HotkeyArguments e)
        {
            if (e.Hotkey == Keys.S)
            {
                this.CancelSuspend = !this.CancelSuspend;
            }
        }
        #endregion

        #endregion

        #region speakmethod
        public new void Speak(string message, bool ignorecancel = false)
        {
            if (this.mutebtn.Checked)
            {
                return;
            }
            if (ignorecancel)
                this.speech.SpeakAsyncCancelAll();

            this.speech.SpeakAsync(message);
        }
        #endregion

        //the button event for the firewall is here aswell.
        #region suspendmethods

        public bool ProcessHooked { get; set; } = false;

        public void DoSuspend()
        {
            Synchronized(delegate
            {
                this.Icon = Properties.Resources.icon_closed;
                this.notifyIcon1.Icon = this.Icon;
                this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: session locked.";

            });

            if (this.suspendprocess != null)
            {
                Synchronized(delegate
                {
                    this.lastaction.Text = "closing sessions, with suspension";
                    this.RegisterKey(Keys.S);
                });
            }
            else
            {
                Synchronized(delegate
                {
                    this.lastaction.Text = "error: suspension process empty";
                });
            }

            suspendworker_DoWorkV2();
        }

        public void CloseSuspendProcess()
        {
            if (this.suspendprocess != null && !this.suspendprocess.HasExited)
            {
                this.suspendprocess.Kill();
                this.suspendprocess = null;
                Synchronized(delegate
                {
                    this.hook.Text = "process drag:";
                });

            }
        }

        private void suspendworker_DoWorkV2()
        {
            if (this.suspendprocess != null && !this.suspendprocess.HasExited)
            {

                Suspend.SuspendProcess(this.suspendprocess.Id);

                Synchronized(delegate
                {
                    this.Cursor = Cursors.WaitCursor;
                });


                Speak("Press the S key to resume the process");

                //Task.Run(() => {
                try
                {
                    while (!this.CancelSuspend) { } //lock
                }
                catch (Exception r)
                {
                    //not going to happen
                }
                finally
                {
                    Suspend.ResumeProcess(this.suspendprocess.Id);
                    this.CancelSuspend = !this.CancelSuspend;
                }
                //})
            }
            Synchronized(delegate
            {
                this.UnRegisterAll();
                RegisterHotKeys();
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.suspendprocess != null && !this.suspendprocess.HasExited)
            {
                this.DoSuspend();
            }
            if (this.fwignore.Checked)
                return;

            this.ToggleFirewall();
        }

        #endregion

        #region networkclosemethod

        public bool NetReleased { get; set; } = false;

        public async void ToggleNetwork()
        {

            ProcessStartInfo pin = new ProcessStartInfo("cmd");
            pin.UseShellExecute = false;
            pin.CreateNoWindow = true;
            if (this.Debug)
            {
                pin.RedirectStandardOutput = true;
                pin.RedirectStandardError = true;
                pin.ErrorDialog = true;

            }


            if (this.NetReleased)
            {

                Synchronized(delegate //synchronize thread.
                {
                    this.Cursor = Cursors.WaitCursor;

                    if (this.fw.HasRules())
                    {
                        this.Icon = Properties.Resources.icon_closed;
                        this.notifyIcon1.Icon = this.Icon;
                        this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: session locked.";
                        this.progress.Value = 100;
                        this.lastaction.Text = "closing sessions";

                            //this.notifyIcon1.BalloonTipText = "The sessions are locked!";
                        }
                    else
                    {
                        this.Icon = Properties.Resources.icon;
                        this.notifyIcon1.Icon = this.Icon;
                        this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: normal.";
                        this.progress.Value = 0;
                        this.lastaction.Text = "opening sessions";

                            //this.notifyIcon1.BalloonTipIcon = Properties.Resources.icon;
                            // this.notifyIcon1.BalloonTipText = "everything is open";
                        }
                    this.Cursor = Cursors.Default;
                });

                Speak("enabling internet connection");

                pin.Arguments = "/c wmic path win32_networkadapter where PhysicalAdapter=True call enable";
            }
            else
            {
                Synchronized(delegate //synchronize thread
                {
                    this.Cursor = Cursors.WaitCursor;

                    this.Icon = Properties.Resources.icon_disconnected;
                    this.notifyIcon1.Icon = this.Icon;
                    this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: Network disabled" + (this.wificheckbox.Checked ? ", wifi disabled." : ".");
                    this.progress.Value = 100;
                    this.lastaction.Text = "network disabled"+(this.wificheckbox.Checked ? ", and wifi disabled" : "");

                        //this.notifyIcon1.BalloonTipText = "The internet connection has been disabled";
                     Speak("disabling internet connection");

                    pin.Arguments = "/c wmic path win32_networkadapter where PhysicalAdapter=True call disable";
                });
            }

            //toggle.
            this.NetReleased = !this.NetReleased;

            string tmp_folder = Path.GetTempPath();
            string log_dir = "sessiondestroyer";
            string file_name = "sessiondestroyer_{0}_{1}.log";

            if (!Directory.Exists(tmp_folder + log_dir))
                Directory.CreateDirectory(tmp_folder + Path.DirectorySeparatorChar + log_dir);

            string path = tmp_folder + log_dir + Path.DirectorySeparatorChar + String.Format(file_name, (pin.Arguments.Contains("disable") ? "wmic_disable" : "wmic_enable"), DateTime.Today.ToString(@"dd/MM/yyyy HH-mm-ss"));
            pin.Arguments += " > \"" + path + "\"";
            Process p = Process.Start(pin);

            p.WaitForExit();

            if (!this.NetReleased)
            {
                if(wificheckbox.Checked)
                {
                    await Task.Delay(TimeSpan.FromSeconds(2)); //gracefully wait 10 seconds so the wifi interface had time to restore..
                    //be so nice to re-connect wifi aswell :)
                    ProcessStartInfo pinfo = new ProcessStartInfo("netsh");
                    pinfo.Arguments = "wlan connect "+this.wifitextbox.Text;
                    pinfo.UseShellExecute = false;
                    pinfo.CreateNoWindow = true;
                    Process proc = Process.Start(pinfo);
                    proc.WaitForExit();
                }
            }

            //File.WriteAllText(tmp_folder + log_dir + Path.DirectorySeparatorChar + String.Format(file_name, (pin.Arguments.Contains("/rel") ? "ipconfig-release" : "ipconfig-renew"), DateTime.Today.ToString(@"dd/MM/yyyy HH-mm-ss")), output);


            Synchronized(delegate
            {
                this.Cursor = Cursors.Default;
            });

            //now we are done KeyTimeOut for some minutes :)
            await Task.Delay(KeyTimeOut);
        }
        #endregion

        #region firewallmethods
        public async void CleanFirewall()
        {
            Synchronized(delegate //synchronize thread
            {
                this.lastaction.Text = "opening session";
                this.Icon = Properties.Resources.icon;
                this.notifyIcon1.Icon = this.Icon;
                this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: normal.";
                fw.RemoveRules();
                progress.Value = 0;
            });
            //Thread.Sleep(KeyTimeOut);
            await Task.Delay(KeyTimeOut);
        }

        private void ToggleFirewall()
        {
            Synchronized(delegate
            {
                this.Cursor = Cursors.Default;
                progress.Value = 50;
            });

            MyFirewall fw = new MyFirewall();
            fw.BlockPorts(1000, 65535, (
              this.udp.Checked && this.tcp.Checked ? MyFirewall.FW_BOTH :
                this.udp.Checked ? MyFirewall.UDP :
                this.tcp.Checked ? MyFirewall.TCP : MyFirewall.UDP
                ), IDGenerator.GetFactory().GetAllIpsFromList(this.whitelist), IDGenerator.GetFactory().GetAllIpsFromList(this.whitelist2));

            Synchronized(delegate
            {
                progress.Value = 100;
            });


            if (this.DestroyKey)
            {
                Synchronized(delegate
                {
                    if (this.suspendprocess != null && !this.suspendprocess.HasExited)
                    {
                        lastaction.Text = "session has been closed, with suspension";
                    }
                    else
                    {
                        lastaction.Text = "session has been closed, without suspension";
                    }
                });

                this.DestroyKey = false;
                Speak("session has been successfully destroyed!");
            }
        }
        #endregion

        #region UIevents
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.notifyIcon1.Visible = true;
            if (this.fw.HasRules())
            {
                this.Icon = Properties.Resources.icon_closed;
                this.notifyIcon1.Icon = this.Icon;
                this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: session locked.";
            }
            else
            {
                this.Icon = Properties.Resources.icon;
                this.notifyIcon1.Icon = this.Icon;
                this.notifyIcon1.Text = "show sessiondestroyer\n\nStatus: normal.";
            }
            this.notifyIcon1.BalloonTipTitle = "SessionDestroyer";
            this.notifyIcon1.BalloonTipText = "you can open the window here in the system tray.";
            this.notifyIcon1.ShowBalloonTip(300);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CleanFirewall();
        }

        private void panel4_Click(object sender, EventArgs e)
        {
            Process.Start("explorer", "https://gitlab.com/xize/sessiondestroyer");
        }

        private void panel4_MouseEnter(object sender, EventArgs e)
        {
            this.panel7.BackColor = Color.FromArgb(43, 43, 43);
        }

        private void panel7_MouseLeave(object sender, EventArgs e)
        {
            this.panel7.BackColor = Color.FromArgb(47, 47, 47);
        }

        private void targetover(object sender, EventArgs e)
        {
            this.targeticon.BackColor = Color.FromArgb(43, 43, 43);
        }

        private void targetleave(object sender, EventArgs e)
        {
            this.targeticon.BackColor = Color.Black;
        }

        private void targeticon_Click(object sender, EventArgs e)
        {
            if (ProcessHooked)
            {
                DialogResult r = MessageBox.Show("Do you want to empty the process hook?", "SessionDestroyer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                if (r == DialogResult.Yes)
                {
                    this.currentid = 0;
                    this.suspendprocess = null;
                    this.hook.Text = "process drag:";
                    this.ProcessHooked = false;
                    return;
                }
                else if (r == DialogResult.Cancel)
                {
                    return;
                }
            }
            GetWindowThreadProcessId(GetForegroundWindow(), out this.currentid);
            MessageBox.Show("click on OK, and select the window you want to get the process from", "help", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.worker.RunWorkerAsync();
        }


        private void worker_work(object sender, DoWorkEventArgs e)
        {
            this.gc.ForceGlobalCross();

            while (true)
            {
                uint a = 0;
                GetWindowThreadProcessId(GetForegroundWindow(), out a);

                if (a != 0 && a != this.currentid)
                {
                    e.Cancel = true;
                    break;
                }
            }
        }

        private void worker_completed(object sender, RunWorkerCompletedEventArgs e)
        {
            uint a = 0;
            GetWindowThreadProcessId(GetForegroundWindow(), out a);


            this.suspendprocess = Process.GetProcessById((int)a);

            Speak("suspension process: " + this.suspendprocess.ProcessName + " hooked!");

            this.hook.Text = "" + this.suspendprocess.ProcessName + ".exe+" + this.suspendprocess.Id;
            this.gc.UndoGlobalCross();
            this.ProcessHooked = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.lastaction.Text = "closing process";
            CloseSuspendProcess();
        }

        private void onclosefixcursor(object sender, FormClosingEventArgs e)
        {
            this.gc.UndoGlobalCross();
            this.con.SaveData(this.wificheckbox, this.wifitextbox, this.whitelist, this.whitelist2);
        }

        private void idlabel_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.idlabel.Text.Substring("Your ID: ".Length));
            MessageBox.Show("your identity has been copied!", "the id has been copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void oncopylocalid(object sender, EventArgs e)
        {
            Clipboard.SetText(this.localid.Text.Substring("Your local ID: ".Length));
            MessageBox.Show("your local identity has been copied!", "the id has been copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void idchangeevent(object sender, EventArgs e)
        {
            int start = this.id.SelectionStart;
            int end = this.id.SelectionLength;

            String text = this.id.Text.Substring(start, end);
            if (text == "<id>")
            {
                this.id.ResetText();
            }
        }

        private void id2changeevent(object sender, EventArgs e)
        {
            int start = this.id2.SelectionStart;
            int end = this.id2.SelectionLength;

            String text = this.id2.Text.Substring(start, end);
            if (text == "<id>")
            {
                this.id2.ResetText();
            }
        }


        private void nickname_Click(object sender, EventArgs e)
        {
            if (this.nickname.Text == "<nickname>")
            {
                this.nickname.ResetText();
            }
        }

        private void nickname2_Click(object sender, EventArgs e)
        {
            if (this.nickname2.Text == "<nickname>")
            {
                this.nickname2.ResetText();
            }
        }

        private void idclick(object sender, EventArgs e)
        {
            if (this.id.Text == "<id>")
            {
                this.id.ResetText();
            }
        }

        private void id2click(object sender, EventArgs e)
        {
            if (this.id2.Text == "<local id>")
            {
                this.id2.ResetText();
            }
        }

        private void nickname_TextChanged(object sender, EventArgs e)
        {
            int start = this.nickname.SelectionStart;
            int end = this.nickname.SelectionLength;

            String text = this.nickname.Text.Substring(start, end);
            if (text == "<nickname>")
            {
                this.nickname.ResetText();
            }

        }

        private void nickname2_TextChanged(object sender, EventArgs e)
        {
            int start = this.nickname2.SelectionStart;
            int end = this.nickname2.SelectionLength;

            String text = this.nickname2.Text.Substring(start, end);
            if (text == "<nickname>")
            {
                this.nickname2.ResetText();
            }

        }

        private void delbtn_Click(object sender, EventArgs e)
        {
            SelectedListViewItemCollection selected = whitelist.SelectedItems;
            if (selected != null)
            {
                foreach (ListViewItem test in selected)
                {
                    test.Remove();
                }
            }
        }

        private void delbtn2_Click(object sender, EventArgs e)
        {
            SelectedListViewItemCollection selected = whitelist2.SelectedItems;
            if (selected != null)
            {
                foreach (ListViewItem test in selected)
                {
                    test.Remove();
                }
            }
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            String pattern = @"^([a-zA-Z0-9=])";
            Regex regex = new Regex(pattern);

            String nickname = this.nickname.Text.Replace(" ", "");
            String identity = this.id.Text.Replace(" ", "");


            if (regex.IsMatch(nickname) && regex.IsMatch(identity))
            {

                IPAddress test = null;
                bool validip = IPAddress.TryParse(IDGenerator.GetFactory().DecryptID(identity), out test);
                if (!validip)
                {
                    MessageBox.Show("the given identity was invalid", "invalid id", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ListViewItem item = new ListViewItem();
                item.Text = nickname;
                item.ForeColor = Color.White;
                item.SubItems.Add(identity);
                this.whitelist.Items.Add(item);
            }
            else
            {
                MessageBox.Show("you are only allowed to use the following pattern:\n\nA-Za-z0-9=", "illegal characters found!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.HideHorizontalScrollBar();

        }

        private void addbtn2_Click(object sender, EventArgs e)
        {
            String pattern = @"^([a-zA-Z0-9=])";
            Regex regex = new Regex(pattern);

            String nickname = this.nickname2.Text.Replace(" ", "");
            String identity = this.id2.Text.Replace(" ", "");


            if (regex.IsMatch(nickname) && regex.IsMatch(identity))
            {

                IPAddress test = null;
                bool validip = IPAddress.TryParse(IDGenerator.GetFactory().DecryptID(identity), out test);
                if (!validip)
                {
                    MessageBox.Show("the given identity was invalid", "invalid id", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ListViewItem item = new ListViewItem();
                item.Text = nickname;

                item.ForeColor = Color.White;
                item.SubItems.Add(identity);
                this.whitelist2.Items.Add(item);
            }
            else
            {
                MessageBox.Show("you are only allowed to use the following pattern:\n\nA-Za-z0-9=", "illegal characters found!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.HideHorizontalScrollBar();

        }

        private void whitelist_VisibleChanged(object sender, EventArgs e)
        {
            HideHorizontalScrollBar();
        }

        private void whitelist_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            HideHorizontalScrollBar();
        }

        private void whitelist_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            HideHorizontalScrollBar();
        }

        private void collapsebtn_Click(object sender, EventArgs e)
        {
            if (this.Collapse)
            {
                //this.Width = 214;
                this.winclosebtn2.Hide();
                this.minimizebtn2.Hide();
                this.winclosebtn.Show();
                this.minimizebtn.Show();
                this.collapsebtn.Text = ">";
                this.roundedcornercontrol.Update();
            }
            else
            {
                //this.Width = 500;
                this.winclosebtn.Hide();
                this.minimizebtn.Hide();
                this.winclosebtn2.Show();
                this.minimizebtn2.Show();
                this.collapsebtn.Text = "<";
                this.roundedcornercontrol.Update();
            }
            this.Collapse = !this.Collapse;
        }

        private void saveclickevent(object sender, EventArgs e)
        {
            this.con.SaveData(this.wificheckbox, this.wifitextbox, this.whitelist, this.whitelist2);
            SaveMessageBox savebox = new SaveMessageBox();
            savebox.ShowDialog();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public void DoCollapse()
        {
            if (this.Collapse)
            {
                this.Width = 214;
                this.winclosebtn2.Hide();
                this.minimizebtn2.Hide();
                this.winclosebtn.Show();
                this.minimizebtn.Show();
                this.collapsebtn.Text = ">";
                this.roundedcornercontrol.Update();
            }
            else
            {
                this.Width = 500;
                this.winclosebtn.Hide();
                this.minimizebtn.Hide();
                this.winclosebtn2.Show();
                this.minimizebtn2.Show();
                this.collapsebtn.Text = "<";
                this.roundedcornercontrol.Update();
            }
            this.Collapse = !this.Collapse;
        }

        private void OnDisablenetwork(object sender, EventArgs e)
        {
            ToggleNetwork();
        }

        private void OnButtonEnterEvent(object sender, EventArgs e)
        {
            this.DoButtonFix((Button)sender, true);
        }

        private void OnButtonLeaveEvent(object sender, EventArgs e)
        {
            this.DoButtonFix((Button)sender, false);
        }

        private void DoButtonFix(Button b, bool highlighted)
        {
            if (b.Text.StartsWith("F1"))
            {
                if (!highlighted)
                {
                    pic1.BackColor = Color.Transparent;
                }
                else
                {
                    pic1.BackColor = Color.FromArgb(0, 74, 153);
                }
            }
            else if (b.Text.StartsWith("F2"))
            {
                if (!highlighted)
                {
                    pic2.BackColor = Color.Transparent;
                }
                else
                {
                    pic2.BackColor = Color.FromArgb(0, 74, 153);
                }
            }
            else if (b.Text.StartsWith("F3"))
            {
                if (!highlighted)
                {
                    pic3.BackColor = Color.Transparent;
                }
                else
                {
                    pic3.BackColor = Color.FromArgb(0, 74, 153);
                }
            }
            else if (b.Text.StartsWith("F4"))
            {
                if (!highlighted)
                {
                    pic4.BackColor = Color.Transparent;
                }
                else
                {
                    pic4.BackColor = Color.FromArgb(0, 74, 153);
                }
            }
        }

        private void notifyIcon1_Click(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Show();
                this.notifyIcon1.Visible = false;
            }
        }

        private void contextclosebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void contextcloseprocbtn_Click(object sender, EventArgs e)
        {
            CloseSuspendProcess();
        }

        private void contextdisablenetbtn_Click(object sender, EventArgs e)
        {
            ToggleNetwork();
        }

        private void contextclosesessionbtn_Click(object sender, EventArgs e)
        {
            DoSuspend();
        }

        private void contextopensessionbtn_Click(object sender, EventArgs e)
        {
            CleanFirewall();
        }
        #endregion

        private void wificheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (wificheckbox.Checked)
            {
                this.wifitextbox.Enabled = true;
            } else
            {
                this.wifitextbox.Enabled = false;
            }
        }

        private void wifitextbox_Click(object sender, EventArgs e)
        {
            this.wifitextbox.Clear();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            WifiProfileWindow wifip = new WifiProfileWindow(this.wifitextbox);
            wifip.ShowDialog();
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        private int WM_LBUTTONDOWN = 0x0201;
        private int WM_LBUTTONUP = 0x0202;

        private void fwignore_CheckStateChanged(object sender, EventArgs e)
        {
            if (!this.fwignore.Checked)
                return;

            SendMessage(targeticon.Handle, WM_LBUTTONDOWN, 0, 1);
            SendMessage(targeticon.Handle, WM_LBUTTONUP, 0, 0);
        }
    }
}
