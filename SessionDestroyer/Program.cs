﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //end loading assembly
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Window win = new Window();

            if(args.Length == 1)
            {
                if (args[0].ToLower() == "debug")
                    win.CDebug = 1;

            }
            
            Application.Run(new Window());
        }
    }
}
