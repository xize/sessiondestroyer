﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionDestroyer
{
    public class FileConfiguration
    {
        private String datafolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\0c3\SessionDestroyer";

        public FileConfiguration()
        {
            if (!Directory.Exists(datafolder))
            {
                Directory.CreateDirectory(datafolder);
            }
        }

        public void SaveData(CheckBox check, TextBox t, System.Windows.Forms.ListView remote, System.Windows.Forms.ListView local)
        {

            if (t.Text == "")
            {
                if (!File.Exists(this.datafolder + @"\config.yml"))
                    return;

                File.Delete(this.datafolder + @"\config.yml");
                return;
            }

            if (t.Text != "wifi profile") //do not save wifi when we use default text... just to spare resources.
                File.WriteAllText(this.datafolder + @"\config.yml", "wifi_profile:" + (check.Checked ? 1 : 0) + ":" + t.Text);

            StringBuilder build = new StringBuilder();

            foreach (System.Windows.Forms.ListViewItem item in remote.Items)
            {
                String nickname = item.Text;
                String identity = item.SubItems[1].Text;
                String serialized = Convert.ToBase64String(Encoding.UTF8.GetBytes(nickname + ";" + identity));
                build.AppendLine(serialized);
            }

            File.WriteAllText(this.datafolder + @"\remote_identity.dat", Convert.ToBase64String(Encoding.UTF8.GetBytes(build.ToString())));

            StringBuilder build2 = new StringBuilder();

            foreach (System.Windows.Forms.ListViewItem item in local.Items)
            {
                String nickname = item.Text;
                String identity = item.SubItems[1].Text;
                String serialized = Convert.ToBase64String(Encoding.UTF8.GetBytes(nickname + ";" + identity));
                build2.AppendLine(serialized);
            }

            File.WriteAllText(this.datafolder + @"\local_identity.dat", Convert.ToBase64String(Encoding.UTF8.GetBytes(build2.ToString())));
        }

        public void LoadData(CheckBox check, System.Windows.Forms.TextBox t, System.Windows.Forms.ListView remote, System.Windows.Forms.ListView local)
        {

            if (File.Exists(this.datafolder + @"\config.yml"))
            {
                string wifi = File.ReadLines(this.datafolder + @"\config.yml").FirstOrDefault();

                string[] split = wifi.Split(':');

                int checkb = int.Parse(split[1]);
                string wifistr = split[2];

                check.Checked = (checkb == 1 ? true : false);
                t.Text = wifistr;

                check.Update();
            }

            if (File.Exists(this.datafolder + @"\remote_identity.dat"))
            {

                String[] lines = Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(this.datafolder + @"\remote_identity.dat"))).Split('\n');

                foreach (String line in lines)
                {
                    if (line != "")
                    {
                        String[] data = Encoding.UTF8.GetString(Convert.FromBase64String(line)).Split(';');

                        ListViewItem item = new ListViewItem();
                        item.Text = data[0];
                        item.SubItems.Add(data[1]);
                        remote.Items.Add(item);
                    }
                }
            }

            if (File.Exists(this.datafolder + @"\local_identity.dat"))
            {

                String[] lines = Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(this.datafolder + @"\local_identity.dat"))).Split('\n');

                foreach (String line in lines)
                {
                    if (line != "")
                    {
                        String[] data = Encoding.UTF8.GetString(Convert.FromBase64String(line)).Split(';');

                        ListViewItem item = new ListViewItem();
                        item.Text = data[0];
                        item.SubItems.Add(data[1]);
                        local.Items.Add(item);
                    }
                }
            }
        }

    }

}
