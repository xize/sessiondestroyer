﻿namespace SessionDestroyer.pages
{
    partial class SelectTargetPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectTargetPage));
            this.label1 = new System.Windows.Forms.Label();
            this.listbox = new System.Windows.Forms.ListBox();
            this.applybtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.LightGray;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Target:";
            // 
            // listbox
            // 
            this.listbox.FormattingEnabled = true;
            this.listbox.Location = new System.Drawing.Point(96, 22);
            this.listbox.Name = "listbox";
            this.listbox.Size = new System.Drawing.Size(120, 121);
            this.listbox.TabIndex = 2;
            // 
            // applybtn
            // 
            this.applybtn.Active = false;
            this.applybtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.applybtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.applybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.applybtn.BorderRadius = 0;
            this.applybtn.ButtonText = "Apply";
            this.applybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.applybtn.DisabledColor = System.Drawing.Color.Gray;
            this.applybtn.Iconcolor = System.Drawing.Color.Transparent;
            this.applybtn.Iconimage = ((System.Drawing.Image)(resources.GetObject("applybtn.Iconimage")));
            this.applybtn.Iconimage_right = null;
            this.applybtn.Iconimage_right_Selected = null;
            this.applybtn.Iconimage_Selected = null;
            this.applybtn.IconMarginLeft = 0;
            this.applybtn.IconMarginRight = 0;
            this.applybtn.IconRightVisible = true;
            this.applybtn.IconRightZoom = 0D;
            this.applybtn.IconVisible = true;
            this.applybtn.IconZoom = 90D;
            this.applybtn.IsTab = false;
            this.applybtn.Location = new System.Drawing.Point(131, 159);
            this.applybtn.Name = "applybtn";
            this.applybtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.applybtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.applybtn.OnHoverTextColor = System.Drawing.Color.White;
            this.applybtn.selected = false;
            this.applybtn.Size = new System.Drawing.Size(85, 24);
            this.applybtn.TabIndex = 3;
            this.applybtn.Text = "Apply";
            this.applybtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.applybtn.Textcolor = System.Drawing.Color.White;
            this.applybtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // SelectTargetPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Controls.Add(this.applybtn);
            this.Controls.Add(this.listbox);
            this.Controls.Add(this.label1);
            this.Name = "SelectTargetPage";
            this.Size = new System.Drawing.Size(233, 196);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listbox;
        private Bunifu.Framework.UI.BunifuFlatButton applybtn;
    }
}
