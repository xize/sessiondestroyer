using WixSharp;
using WixSharp.UI.Forms;

namespace WixSharpSetup.Dialogs
{
    partial class ExitDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            this.viewLog = new System.Windows.Forms.LinkLabel();
            this.back = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.roundedCornersControl1 = new SessionDestroyer.controls.RoundedCornersControl();
            this.roundedCornersControl2 = new SessionDestroyer.controls.RoundedCornersControl();
            this.roundedCornersControl3 = new SessionDestroyer.controls.RoundedCornersControl();
            this.runcheckbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.title.BackColor = System.Drawing.Color.Transparent;
            this.title.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.title.ForeColor = System.Drawing.Color.LightGray;
            this.title.Location = new System.Drawing.Point(12, 105);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(299, 27);
            this.title.TabIndex = 6;
            this.title.Text = "[ExitDialogTitle]";
            // 
            // description
            // 
            this.description.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.description.BackColor = System.Drawing.Color.Transparent;
            this.description.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.ForeColor = System.Drawing.Color.DarkGray;
            this.description.Location = new System.Drawing.Point(12, 123);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(298, 26);
            this.description.TabIndex = 7;
            this.description.Text = "[ExitDialogDescription]";
            // 
            // viewLog
            // 
            this.viewLog.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.viewLog.AutoSize = true;
            this.viewLog.BackColor = System.Drawing.Color.Transparent;
            this.viewLog.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewLog.LinkColor = System.Drawing.Color.Gray;
            this.viewLog.Location = new System.Drawing.Point(12, 340);
            this.viewLog.Name = "viewLog";
            this.viewLog.Size = new System.Drawing.Size(52, 11);
            this.viewLog.TabIndex = 1;
            this.viewLog.TabStop = true;
            this.viewLog.Text = "[ViewLog]";
            this.viewLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.viewLog_LinkClicked);
            // 
            // back
            // 
            this.back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.back.AutoSize = true;
            this.back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.back.Enabled = false;
            this.back.FlatAppearance.BorderSize = 0;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.back.ForeColor = System.Drawing.Color.White;
            this.back.Location = new System.Drawing.Point(224, 326);
            this.back.MinimumSize = new System.Drawing.Size(75, 0);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(79, 25);
            this.back.TabIndex = 0;
            this.back.Text = "[WixUIBack]";
            this.back.UseVisualStyleBackColor = false;
            // 
            // next
            // 
            this.next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.next.AutoSize = true;
            this.next.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.next.FlatAppearance.BorderSize = 0;
            this.next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.next.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.next.ForeColor = System.Drawing.Color.White;
            this.next.Location = new System.Drawing.Point(309, 326);
            this.next.MinimumSize = new System.Drawing.Size(75, 0);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(81, 25);
            this.next.TabIndex = 0;
            this.next.Text = "[WixUIFinish]";
            this.next.UseVisualStyleBackColor = false;
            this.next.Click += new System.EventHandler(this.finish_Click);
            // 
            // cancel
            // 
            this.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancel.AutoSize = true;
            this.cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(74)))), ((int)(((byte)(153)))));
            this.cancel.Enabled = false;
            this.cancel.FlatAppearance.BorderSize = 0;
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.cancel.ForeColor = System.Drawing.Color.White;
            this.cancel.Location = new System.Drawing.Point(395, 326);
            this.cancel.MinimumSize = new System.Drawing.Size(75, 0);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(87, 25);
            this.cancel.TabIndex = 0;
            this.cancel.Text = "[WixUICancel]";
            this.cancel.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightGray;
            this.label1.Location = new System.Drawing.Point(324, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 19);
            this.label1.TabIndex = 15;
            this.label1.Text = "setup completed";
            // 
            // roundedCornersControl1
            // 
            this.roundedCornersControl1.Radius = 8;
            this.roundedCornersControl1.TargetControl = this.back;
            // 
            // roundedCornersControl2
            // 
            this.roundedCornersControl2.Radius = 8;
            this.roundedCornersControl2.TargetControl = this.next;
            // 
            // roundedCornersControl3
            // 
            this.roundedCornersControl3.Radius = 8;
            this.roundedCornersControl3.TargetControl = this.cancel;
            // 
            // runcheckbox
            // 
            this.runcheckbox.AutoSize = true;
            this.runcheckbox.BackColor = System.Drawing.Color.Transparent;
            this.runcheckbox.Font = new System.Drawing.Font("MS UI Gothic", 8.25F);
            this.runcheckbox.ForeColor = System.Drawing.Color.Gray;
            this.runcheckbox.Location = new System.Drawing.Point(14, 152);
            this.runcheckbox.Name = "runcheckbox";
            this.runcheckbox.Size = new System.Drawing.Size(91, 15);
            this.runcheckbox.TabIndex = 16;
            this.runcheckbox.Text = "run application";
            this.runcheckbox.UseVisualStyleBackColor = false;
            // 
            // ExitDialog
            // 
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Setup.Properties.Resources.layout2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(494, 361);
            this.Controls.Add(this.runcheckbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.viewLog);
            this.Controls.Add(this.back);
            this.Controls.Add(this.description);
            this.Controls.Add(this.next);
            this.Controls.Add(this.title);
            this.Controls.Add(this.cancel);
            this.Name = "ExitDialog";
            this.Text = "[ExitDialog_Title]";
            this.Load += new System.EventHandler(this.ExitDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.LinkLabel viewLog;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Label label1;
        private SessionDestroyer.controls.RoundedCornersControl roundedCornersControl1;
        private SessionDestroyer.controls.RoundedCornersControl roundedCornersControl2;
        private SessionDestroyer.controls.RoundedCornersControl roundedCornersControl3;
        private System.Windows.Forms.CheckBox runcheckbox;
    }
}